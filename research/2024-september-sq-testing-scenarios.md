# 2024 September usability testing scenarios

## Summary
This document is for usability testing participants, it outlines the tasks contained in the usability testing session. It does not contain solutions, or how the participant should complete them.

It is for participants to refer to, if necessary, during usability testing sessions. 

## Tmate help

For these tasks we will be using Tmate a terminal sharing service. If you need to scroll backward in the terminal to see earlier output you can use:

```
Ctrl-b then [
'q' is for leaving this mode
```

## Task 1 - Create a key and publish its certificate

**Scenario**

You are Shirley Beverly, a new maintainer of an open source project, Foo. The project uses sequoia-pgp for its file and email encryption needs.

**Part 1**

Create a OpenPGP key for "Shirley Beverly" whose email address is shirley.beverly@example.com. The expiration should be set to three years.

**Part 2**

When you are happy with the key, mark the certificate as trust introducer.

**Part 3**

Publish it on hkps://keys.openpgp.org.

## Task 2 - Verify a file download

**Scenario**

Software 'Foo' is released on platform 'example.com'. 'Foo' has 3 project managers: Alice, Bob and Carol. At least one of them signs releases on that platform.

There is also a certificate authority (CA) which signed the certificates of Alice, Bob and Carol.

The corresponding certificates can be found at:

- https://example.com/certificates/alice.pgp
- https://example.com/certificates/bob.pgp
- https://example.com/certificates/carol.pgp
- https://example.com/certificates/ca.pgp

**Part 1**

Download the software 'Foo' version 1.0. It's available at https://example.com/foo-1.0.tar.gz.
One of the project's release managers signed the tarball.  
The detached signature is available at https://example.com/foo-1.0.tar.gz.sig.

Download the files using `wget $URL`.
Make sure they are imported and authenticated.
Now verify the file.



**Part 2**

It's six months later, and the project has released version 1.1. Download the new version and verify it again.

This new version is available at https://example.com/foo-1.1.tar.gz.  
The detached signature is available at https://example.com/foo-1.1.tar.gz.sig.

## Task 3 - Write an encrypted e-mail to Dave

**Scenario**

You want to send Dave encrypted mail using `sequoia-pgp`. The mail doesn't contain anything particularly sensitive, but you care about your privacy so you would prefer to encrypt it.

Dave told you that he published a certificate for `<dave@example.com>` on a keyserver.  

**Part 1**

Get his certificate, analyse the results and encrypt a message to Dave. For this task, don't worry about signing the message.


## Task 4 - Share Daves certificate as authentic

**Scenario**

Later you meet Dave and he gave you the fingerprint of his certificate.

Part 1 

Check that fingerprint

Part 2

Alice asks you for Daves certificate. Since you checked the fingerprint you are certain, you have the right one. 

Mark/augment the certificate, so that Alice can easily authenticate it, because she trusts you.

