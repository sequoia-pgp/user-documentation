# Research plan

## 0. Considerations

* We want to record the tests/testees - what exactly are we recording, which recordings are needed 
or wishful? 1) screen captures (of course) but also 2) audio tracks if we encourage people to think 
aloud. How to synchronize these two? Do we need actual video (moving picture)? That might scare people...
* We should avoid testees stumbling over previous test runs - like already published keys. Suggestion: 
Keep things as local as possible.
* Should testees get access to the user documentation or do they work with the help pages? They should test the software and the help pages, not the user manual.

## 1. Questions: what do we want to learn?

* How easily can GnuPG users transition to sq?
* How easy it is for people to find sq commands?
* Is the output or the structure of commands and subcommands of `sq` leading people in
  the right direction or is it distracting or overwhelming.

## 2. Participants

### 2.1 Participant profile

* Our participants will be software engineers who contribute to the Debian project or security trainers. 
* We expect them to be security conscious and, to some degree, aware of good security practices. 
* We expect them to be users of GnuPG, although it is unclear what will be their depth of knowledge and expertise.
* We do not expect participants to have experience with sq.

### 2.2 Participant numbers

* 5 to 10 participants.

### 2.3 Recruitment approach

* Some easy ways to recruit participants:
    ** Post through the sq Mastodon account
    ** Blogpost in the sq website
    ** Add a 'research' page to the sq website 
    ** Post on the mailing list and IRC channels (ask people if anyone they know may be interested in taking part)
* Recruitment *during* conferences:
    ** Announce at the end of any talk / session by sq contributors
    ** Print some fliers and post them around
    ** Mention it to every single person you meet / talk to.
* Participants only need to provide an email address where we can contact them to make arrangements for the session.

### 2.4 Consent approach

* Verbal consent before the session should suffice.
* Since we would like to video record the sessions, we must specifically gather consent for recording.
* If participant agrees to recording, record verbal consent. 

### 2.5 Compensation

* There is a budget for compensation.
* We should treat participation in user research as a contribution to sq. 
* It is good practice to compensate participants as a thank you for their time and expertise. 
* Compensation can take many forms:
    ** A gift card for a token amount (e.g. 25 or 30 euro). 
    ** Adding participants to contributor lists, release notes, and patches implementing improvements derived from research. 
    ** Swag (mugs?).

## 3. Tasks and scenarios

From the originally proposed tasks 1,2 and 4 are taken. Pilot should be generated and tested to determine if the amount of time to solve the tasks is within the expected range. 

### 1. Create a key and publish its certificate

#### Task

1.) Create a OpenPGP key for *Shirley Beverly* whose email
address is `shirley.beverly@example.com`. The expiry should be set to three years.
2.) When you are happy with it, mark the certificate as
trust introducer, and then 3) publish it on `hkps://keys.openpgp.org`.

> Note: We setup fake (local) keyservers for all default keyservers of `sq`. Publishing even if unconstrained isn't effecting the outer world.
> The fake keyservers are non-authenticating - meaning no approval emails are emitted.

#### Setup

The user will be provided with `sq`. 

#### Solution

1.  Best:

    ```
    sq key generate --name 'Shirley Beverly' --email 'shirley.beverly@example.com'
    ```

    Good:

    ```
    sq key generate --userid 'Shirley Beverly shirley.beverly@example.com'
    ```

    The certificate is set to expire after three years by default.
    This should be evident in `sq key generate`'s output.  Otherwise,
    users can see that is the case using `sq inspect --cert CERT`.

2. `sq pki link authorize --unconstrained --cert $FINGERPRINT --all`

3. `sq network keyserver publish --server 'hkps://keys.openpgp.org' --cert $FINGERPRINT`


### 2. Verify a Download

#### Task

Software 'Foo' is released on platform 'example.com'. 
'Foo' has 3 project managers: Alice, Bob and Carol. At least one of them
signs releases on that platform. 

There is also a certificate authority (CA) which signed the certificates 
of Alice, Bob and Carol.

The corresponding certificates can be found at
- https://example.com/certificates/alice.pgp
- https://example.com/certificates/bob.pgp
- https://example.com/certificates/carol.pgp
- https://example.com/certificates/ca.pgp

1.) Download the software 'Foo', version 1.0.
It's available at <https://example.com/foo-1.0.tar.gz>.  
One of the project's release managers signed the tarball.  
The detached signature is available at
<https://example.com/foo-1.0.tar.gz.sig>

Download the files using `wget $URL`.
Make sure they are imported and authenticated.
Now verify the file. 


2.) It's six months later, and the project has released version 1.1.
Download the new version and verify it again.

It's available at <https://example.com/foo-1.1.tar.gz>.    
The detached signature is available at
<https://example.com/foo-1.1.tar.gz.sig>


#### Setup

There is a local webserver listening on 'example.com'. All files are
already present on that local server.

As the image for the test scenarios has no webbrowser, there is no page 
explaining the directory structure - this has to be done in the description of
the task.

#### Solution

1. (A) Best:
 ```
   wget https://example.com/foo-1.0.tar.gz
   wget https://example.com/foo-1.0.tar.gz.sig

   sq network search https://example.com/certificates/alice.pgp
   sq network search https://example.com/certificates/bob.pgp
   sq network search https://example.com/certificates/carol.pgp
   sq network search https://example.com/certificates/ca.pgp
```

> Note: In case the user doesn't realize that `sq network search` works with plain URLs, 
an alternate way of getting the certificate would be

  ```shell
  wget https://example.com/certificates/alice.pgp
  sq cert import alice.pgp
  ... (also for the rest of the certificates)
  ```


   ```
   sq pki link authorize --domain=example.com --cert=$FINGERPRINT_CA depth=1

   sq verify foo-1.0.tar.gz --detached foo-1.0.tar.gz.sig
   ```

   (B) Alternative (no use of CA):

   ```
   wget https://example.com/foo-1.0.tar.gz
   wget https://example.com/foo-1.0.tar.gz.sig
   sq verify foo-1.0.tar.gz --detached foo-1.0.tar.gz.sig
   # Based on error that the public key for ALICE_KEYID is missing.

   sq network search https://example.com/certificates/alice.pgp
   sq verify --signature-file foo-1.0.tar.gz.sig foo-1.0.tar.gz
   # This says the signature was made by ALICE_FPR, but ALICE_FPR
   # couldn't be authenticated.  So, this is sufficient, but
   # better is:

   sq pki link add --all $ALICE_FINGERPRINT
   sq verify signature-file foo-1.0.tar.gz.sig foo-1.0.tar.gz
   # Now sq is happy that it is authenticate.
   ```



2. (A) If the users said they were willing to rely on the CA:

   ```
   wget https://example.com/foo-1.1.tar.gz
   wget https://example.com/foo-1.1.tar.gz.sig
   sq verify foo-1.1.tar.gz --detached foo-1.1.tar.gz.sig
   ```

   (B) If the user did not authenticate the right one or any certificates:

   ```
   wget https://example.com/foo-1.1.tar.gz
   wget https://example.com/foo-1.1.tar.gz.sig
   sq verify signature-file foo-1.1.tar.gz.sig foo-1.1.tar.gz
   # Signed by Carol, not Alice!  Proceed as for one.
   ```


### 4. Send an Encrypted Mail to Dave (Find the right certificate for Dave)

#### Task

1.) You want to send Dave encrypted mail using `sq`.  The mail
doesn't contain anything particularly sensitive, but you care about
your privacy so you would prefer to encrypt it.  Dave told you that
he published a certificate for `<dave@example.com>` on a keyserver.  
Get his certificate, analyse the results and encrypt a message to Dave.  
(For this task, don't worry about signing the message.)  


#### Setup

`keys.openpgp.org` contains a certificate, which is verified for
Dave, but there are also a few on the SKS key servers.  The user
needs to choose the right one, and should mark it as authenticated.

#### Solution

(A)

```
$ sq network search dave@example.com
Note: Created a local CA to record provenance information.
Note: See `sq pki link authorize` and `sq pki link --help` for more information.

Importing 2 certificates into the certificate store:

  1. 417B9A4EBBEBD2E9D759DB6E5DFE72A74B661995 Dave (partially authenticated, 3/120)
  2. 27CBB7073A27F84985D28CA99E7AAC3D67FE2403 Dave (UNAUTHENTICATED)

Imported 2 new certificates, updated 0 certificates, 0 certificates unchanged, 0 errors.

After checking that a certificate really belongs to the stated owner, you can mark the certificate
as authenticated using:

    sq pki link add FINGERPRINT

$ sq pki identify $DAVE_FINGERPRINT
417B9A4EBBEBD2E9D759DB6E5DFE72A74B661995 will expire on 2027-08-13T07:41:41Z
  [   3/120 ] <dave@example.com>
  [   3/120 ] <dave@foo.org>
  [   3/120 ] Dave

To view why a user ID is considered valid, pass `--show-paths`

To see more details about a certificate, run:

  $ sq inspect --cert FINGERPRINT

3 bindings found.
Skipped 3 bindings, which could not be authenticated.
Pass `--gossip` to see the unauthenticated bindings.
Error: Could not authenticate any paths.

$ sq pki link add --cert=$DAVE_FINGERPRINT --email=dave@example.com
Linking 417B9A4EBBEBD2E9D759DB6E5DFE72A74B661995 and dave@example.com.


(B) Alternative solution: User trusts keys.openpgp.org (KOO) completely: First mark the shadow ca of KOO
  as a fully trusted introducer, then the certificate is
  completely authenticated.
```

```
$ sq network fetch dave@example.com
$ sq pki identify $FIRST_CERT_FINGERPRINT
$ sq pki identify $SECOND_CERT_FINDERPRINT
$ sq pki link authorize --unconstrained --cert=$SHADOW_CA_KOO_FINGERPRINT
$ sq inspect --cert $DAVE_IDENTIFIED_FINGERPRINT --certifications
```




## 4. Method

### 4.1 Approach

* Usability study
* Moderated
* Think aloud

### 4.2 Equipment

* Computer
* External keyboard and mouse
* Configured as required for the chosen tasks / scenarios
* Video recording software

## 5. Data collection

If we want to publish something in the future, we will need the sessions video recorded.

## 6. Required documentation

* Recruitment leaflet
* Research page on sq website
* Blogpost
* Social media post
* Participant information sheet
** Since sessions run with tmate, tell users to scroll with 'Ctrl-b' followed by 'AltGr-[' keys
* Test script
