# Intro

Thanks `$participant` for agreeing to take part in our research. The purpose of this research is to understand how well the commands, and command structure, in sequoia-pgp (sq) works for users.

**Explanation of session**

Have you ever taken part in a usability testing session before? I'll explain what will happen.

The session will take at most 1 hour. We will begin with some short questions about your use of GnuPG.

We will then ask you to try 3 things with sq. You'll use our test infrastructure for these. I will give you more detail shortly.

During this call we'll ask you to share your screen so we can obseve the terminal window you're using. If you are willing, we'd like you to share your camera also.

**Attendees**

I will be facilitating the session today, I am a usability researcher and I contribute to open source software. `$Observer1` and `$ObserverN` will be observing. They may have questions for you, which they'll ask at the end of the session.

**Think aloud**

The next 3 things I want to tell you are very important. 

1. In order for us to identify what we must improve, it's really important that you give us honest feedback. If there is something confusing, or you expected it to work in a different way, tell us. I may ask you to give me more detail during the session.

2. As you are working through each task, I'd like you to tell me what you are thinking about the task - speak your internal thoughts about the task, and how you think you need to achieve them, aloud.

If you get stuck on something, I want you to verbalise what you intend on trying and what you expect will happen.

3. We are testing the software, not you - there are no right or wrong answers. You can stop at any moment you'd like. 

Any questions before we start?

## GnuPG introduction

I'd like to understand a little bit about your use of GnuPG. 
- How long have you been using GnuPG?
- What is your main usage of GnuPG?
- When you need help or support with GPG, what do you do to get support?

Now I'll explain the tasks.

# Task introductions

- The project has set-up test infrastructure with a VPS that has sequoia-pgp installed. We'll use this to test the software
- There *may* be a short 1-2 second delay in the terminal meaning the commands you enter may take 1-2 seconds to appear on the screen. I suggest taking your time when typing.
- If you need help, or to refer to documentation, please do so as you would normally. 
- If you have questions about how to complete the tasks once we start, but I will note them and we'll answer at the end.

(If asked:
- it also has a test HKP server for publishing, and discovering keys used in these tests. Unlike a real-world HKP server, this test server will not send approval emails.
- none of the keys you will use will affect the outside world)

Understood? So let's begin. Any questions?

## Task 0

I'd like you to connect to the test infrastructure using SSH and `$hostname`.

You're connected, good. Now, I'd like you to get familiar with the delay in the terminal output. Maybe try some commands `ls`, etc.

You can move backward in the terminal output using:

```
Ctrl-b then [
'q' is for leaving this mode
```

## Task 1 - Create a Certificate

The first task has 3 parts. I will paste the first part of this task into the chat so you can read it. 

**Scenario**

You are Shirley Beverly, a new maintainer of an open source project, Foo. The project uses sequoia-pgp for it's file and email encryption needs.

**First part**

Create a OpenPGP key for "Shirley Beverly" whose email address is shirley.beverly@example.com. The expiry should be set to three years.

So, please try this now.

**Prompt questions**

- Can you tell us when the certificate expires"
(if they can't figure out when it expires, suggest looking at the documentation for a command to use)
- Can you look at the hint "Hint: If this is your key, you should mark it as a fully trusted introducer" - read it out loud for us, and tell us what you think it means.

...

Are you happy with that? OK.

**Second part**

When you are happy with the key, mark the certificate as trust introducer.

**Prompt questions**

- Can you tell me what you think has happened here?

**Third part**

Publish it on hkps://keys.openpgp.org.

Great, we'll move on to task 2.

## Task 2 - Verify a Download

**Intro**

For this task we have set up a local webserver listening on 'example.com'. All the files mentioned here are already present on that local server.

**Scenario**

Software 'Foo' is released on platform 'example.com'. 'Foo' has 3 project managers: Alice, Bob and Carol. At least one of them signs releases on that platform.

There is also a certificate authority (CA) which signed the certificates of Alice, Bob and Carol.

The corresponding certificates can be found at:

- https://example.com/certificates/alice.pgp
- https://example.com/certificates/bob.pgp
- https://example.com/certificates/carol.pgp
- https://example.com/certificates/ca.pgp

**Part 1**

Download the software 'Foo' version 1.0. It's available at https://example.com/foo-1.0.tar.gz.
One of the project's release managers signed the tarball.  
The detached signature is available at https://example.com/foo-1.0.tar.gz.sig.

Download the files using `wget $URL`. Assume the downloaded certificates are authenticated because of their origin. Now tell your system about the authentication by marking them as trust introducers. Afterwards verify the file.

**Prompt questions**

- Can you tell me what you think has happened here?
- Can you tell me if the linking command gives you enough information?

OK, let's move onto part 2. 

**Part 2**

It's six months later, and the project has released version 1.1. Download the new version and verify it again.

This new version is available at https://example.com/foo-1.1.tar.gz.  
The detached signature is available at https://example.com/foo-1.1.tar.gz.sig.

And now we'll move onto the final task.

## Task 3 - Write an encrypted e-mail to Dave

**Scenario**

You want to send Dave encrypted mail using `sequoia-pgp`. The mail doesn't contain anything particularly sensitive, but you care about your privacy so you would prefer to encrypt it.

Dave told you that he published a certificate for `<dave@example.com>` on a keyserver.  

**Part 1**

Get Dave's certificate, and encrypt a message to him. For this task, don't worry about signing the message.


## Wrap-up

Thanks `$participant`, we've come to the end of the session. Is there anything you'd like to tell us?
Observers, do you have any questions for `$participant`?

Possible points to make:
Just to let you know, what will happen next. 

Thank you very much for your time and for taking part. This helps the project immensely to understand what needs to be improved for our users.

If we have more testing to do in the future, can we contact you about it? You're not under any obligation to of course!

Thank you. 
End.
