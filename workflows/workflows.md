﻿# 1. Create a Certificate

## Task

1.) Create a OpenPGP certificate for *Shirley Beverly* whose email
address is `shirley.beverly@probier.email`. It should expire after
three years. Specify the name of your revocation file using the --rev-cert option. 
2.) When you are happy with the certificate, mark it as
yours, and then 3) publish it on `keys.openpgp.org`.

## Setup

The user will be provided with `sq`, Thunderbird, and a web browser.
Thunderbird will already be configured to receive email for
`shirley.beverly@probier.email`.

## Solution

1.  Best:

    ```
    sq key generate --userid 'Shirley Beverly' --userid '<shirley.beverly@probier.email>' --rev-cert='shirley.beverly.pgp.rev'
    ```

    Good:

    ```
    sq key generate --userid 'Shirley Beverly <shirley.beverly@probier.email>' --rev-cert='shirley.beverly.pgp.rev'
    ```

    The certificate is set to expire after three years by default.
    This should be evident in `sq key generate`'s output.  Otherwise,
    users can see that is the case using `sq inspect --cert CERT`.

2. `sq pki link add --ca \* FINGERPRINT --all`

3. `sq network keyserver publish FINGERPRINT`

   or

   `sq cert export FINGERPRINT`, and upload via `keys.openpgp.org`'s
   web interface.

   The user needs to check their email, check the fingerprint, and
   click the link.

# 2. Verify a Download

## Task

1.) Download the latest version of the software Foo, version 2.1.
It's available at <https://example.com/>.  One of the project's
release managers signed the tarball.  Download and authenticate it.
2.) It's six months later, and the project has released version 2.2.
Download the new version and authenticate it.

## Setup

<https://example.com> has a link to a releases page.  The page lists
the releases, and links to the OpenPGP signatures.  The page also says
that releases are signed by one of three keys belonging to
`<alice@example.com>`, `<bob@example.com>`, or `<carol@example.com>`.
Those keys are in turned signed by the project's CA key.  The CA is
also described on the webpage.  Release 2.1 is signed by Alice.
Release 2.2 is signed by Carol.

- Does the user notice that Bob didn't sign any of the tarballs?

- Does the user expect all the release managers to sign each of
  the tarballs?

## Solution

1. Best:

   ```
   wget https://example.com/releases/foo-2.1.tar.gz
   wget https://example.com/releases/foo-2.1.tar.gz.sig

   sq network fetch https://example.com/certificates/alice.pgp
   sq network fetch https://example.com/certificates/bob.pgp
   sq network fetch https://example.com/certificates/carol.pgp
   sq network fetch https://example.com/certificates/ca.pgp

   sq pki link add --ca example.com --all CA_FINGERPRINT

   sq verify foo-2.1.tar.gz --detached foo-2.1.tar.gz.sig
   ```

   Alternative (no use of CA):

   ```
   wget https://example.com/releases/foo-2.1.tar.gz
   wget https://example.com/releases/foo-2.1.tar.gz.sig
   sq verify foo-2.1.tar.gz --detached foo-2.1.tar.gz.sig
   # Based on error that the public key for ALICE_KEYID is missing.
   sq network fetch ALICE_KEYID
   sq verify foo-2.1.tar.gz --detached foo-2.1.tar.gz.sig
   # This says the signature was made by ALICE_FPR, but ALICE_FPR
   # couldn't be authenticated.  So, this is sufficient, but
   # better is:
   sq pki link add --all ALICE_FPR
   sq verify foo-2.1.tar.gz --detached foo-2.1.tar.gz.sig
   # Now sq is happy that it is authenticate.
   ```

2. If the user said they were willing to rely on the CA:

   ```
   wget https://example.com/releases/foo-2.2.tar.gz
   wget https://example.com/releases/foo-2.2.tar.gz.sig
   sq verify foo-2.2.tar.gz --detached foo-2.2.tar.gz.sig
   ```

   If the user only authenticated Alice or did not authenticate
   any certificates:

   ```
   wget https://example.com/releases/foo-2.2.tar.gz
   wget https://example.com/releases/foo-2.2.tar.gz.sig
   sq verify foo-2.2.tar.gz --detached foo-2.2.tar.gz.sig
   # Signed by Carol, not Alice!  Proceed as for one.
   ```

# 3. Fix a Certificate using SHA-1

## Task

Alice says that she fetched your certificate from `keys.openpgp.org`,
but her email program is complaining that it is unusable.  Use `sq` to
figure out what the problem is, and fix it so that Alice can send you
an encrypted mail.

- Variants of the task: certificate is expired.

## Setup

The user is provided with a certificate that uses SHA-1 bindings, and
`sq`.

## Solution

1. Diagnosis:

   ```
   sq cert lint FINGERPRINT
   sq inspect --cert FINGERPRINT
   ```

2. Fix it:

   ```
   sq cert lint --fix FINGERPRINT
   ```

3. Publish the update:

   ```
   sq network keyserver publish FINGERPRINT
   ```

# 4. Send an Encrypted Mail to Alice (Find the right certificate for Alice)

## Task

1.) You want to send Alice encrypted mail using `sq`.  The mail
doesn't contain anything particularly sensitive, but you care about
your privacy so you would prefer to encrypt it.  Alice told you that
she published a certificate for `<alice@example.org>` on
`keys.openpgp.org`.  Get her certificate, and encrypt a message to
her.  (For this task, don't worry about signing the message.)  2.)
It's a few weeks later, encrypt another message to her.

## Setup

`keys.openpgp.org` contains a certificate, which is verified for
Alice, but there are also a few on the SKS key servers.  The user
needs to choose the right one, and should mark it as authenticated.

## Solution

- Alternative solution: user trusts KOO completely: first mark KOO
  as a fully trusted trusted introducer, then the certificate is
  completely authenticated.

```
$ sq network fetch neal@walfield.org
Note: Created a local CA to record provenance information.
Note: See `sq pki link list --ca` and `sq pki link --help` for more information.

Importing 5 certificates into the certificate store:

  1. 8F17777118A33DDA9BA48E62AACB3243630052D9 Neal H. Walfield <neal@sequoia-pgp.org> (partially
authenticated, 3/120)
  2. F7173B3C7C685CD9ECC4191B74E445BA0E15C957 Neal H. Walfield (Code Signing Key)
<neal@pep.foundation> (partially authenticated, 1/120)
  3. 11C294DF1D6C9698FEFE231D3BF609C68BAFCDBD Neal H. Walfield <neal@walfield.org> (UNAUTHENTICATED)
  4. 520054A53C19CBB2E7F5639687234295786B0BAD Neal H Walfield <neal@debian.org> (UNAUTHENTICATED)
  5. A554091A1D1C484FC1058E9ACDB33BB08BAFCDBD <unknown>

Imported 5 new certificates, updated 0 certificates, 0 certificates unchanged, 0 errors.

After checking that a certificate really belongs to the stated owner, you can mark the certificate
as authenticated using:

    sq pki link add FINGERPRINT

$ sq pki identify 8F17777118A33DDA9BA48E62AACB3243630052D9
8F17777118A33DDA9BA48E62AACB3243630052D9 will expire on 2024-08-25T09:00:14Z
  [   2/120 ] Neal H. Walfield <neal@gnupg.org>
  [   2/120 ] Neal H. Walfield <neal@pep-project.org>
  [   1/120 ] Neal H. Walfield <neal@pep.foundation>
  [   3/120 ] Neal H. Walfield <neal@sequoia-pgp.org>
  [   2/120 ] Neal H. Walfield <neal@walfield.org>

To view why a user ID is considered valid, pass `--show-paths`

To see more details about a certificate, run:

  $ sq inspect --cert FINGERPRINT

6 bindings found.
Skipped 6 bindings, which could not be authenticated.
Pass `--gossip` to see the unauthenticated bindings.
Error: Could not authenticate any paths.
$ sq pki identify 8F17777118A33DDA9BA48E62AACB3243630052D9 --show-paths
...
[ ] 8F17777118A33DDA9BA48E62AACB3243630052D9 Neal H. Walfield <neal@walfield.org>: marginally
    authenticated (1%)
...
  Path #2 of 2, trust amount 1:
    ◯ 13FA9F1C986B2A55D4C1F0065D550A7ABD130225 ("Local Trust Root")
    │   partially certified (amount: 40 of 120) the following certificate on 2002-02-20 as a
    │   partially trusted (40 of 120) meta-introducer (depth: unconstrained)
    ├ FD947931DD513E8B5D1490F4C5370322BFB47B42 ("Public Directories")
    │   partially certified (amount: 1 of 120) the following certificate on 2002-02-20 as a
    │   partially trusted (1 of 120) meta-introducer (depth: unconstrained)
    ├ 4AC161923AB96579F235F4F7FD789F1859FFFD9E ("Downloaded from keys.openpgp.org")
    │   certified the following binding on 2024-04-23
    └ 8F17777118A33DDA9BA48E62AACB3243630052D9 "Neal H. Walfield <neal@walfield.org>"

6 bindings found.
Skipped 6 bindings, which could not be authenticated.
Pass `--gossip` to see the unauthenticated bindings.
Error: Could not authenticate any paths.
$ sq pki link add 8F17777118A33DDA9BA48E62AACB3243630052D9 "Neal H. Walfield <neal@walfield.org>"
Linking 8F17777118A33DDA9BA48E62AACB3243630052D9 and "Neal H. Walfield <neal@walfield.org>".
$ sq encrypt --recipient-email neal@walfield.org
```

- Step 2.) A few weeks go by: if the user marked the binding as authenticated,
  then `sq encrypt --recipient-email neal@walfield.org` is enough.  If not,
  the user has to manually authenticate the binding again.

# 5. Certify a Binding

## Task

You met Alice at a conference, and exchanged business cards with her.
Her business card lists her name, her certificate's fingerprint
(ALICEFPR), and her email address, `alice@example.com`.  Get Alice's
certificate, certify the binding with your key (MYFPR), and then send
the certification to Alice.  (Store the message that you would send in
the file `email.txt`.)

- The goal of the task is not so much sending the email at the end, but
  using `sq certify`.

- Perhaps a different formulation is: you went to FOSDEM, got a photo
  of the Qubes release key t-shirt.  Now you want to certify the
  certficate.  At the end, you don't send an email, but publish it
  directly on the key servers.

## Setup

The user is given a copy of the business card as described above.  The
user is provided with a pregenerated key.

## Solution

```
sq network fetch FINGERPRINT
sq inspect --cert FINGERPRINT
sq pki certify FINGERPRINT userid
sq cert export FINGERPRINT > email.txt
```

- Alternative: the user may already have the certificate, so the
  `sq network fetch` step may be unnecessary.

# 6. Encrypt and decrypt a file

## Task

You created a backup of some important files. Encrypt this file with your key. Assume that some time 
has passed and you want to access your backup again. 

## Setup

The user is given a text file 'backup.txt' containing the text "My important data". The runtime environment
contains a keystore with Alice's key (no need to create one).

## Solution

```
sq encrypt --recipient-email alice@example.com backup.txt -o backup.pgp
sq decrypt backup.pgp -o restored.txt
```

# 7. Migrate a key to sequoia-pgp

## Task

You decided to use sequoia-pgp in the future. Assume that you already exported your keys into a file. 
Migrate you keys over to sequoia-pgp. After that, list the keys known to sequoia-pgp.

## Setup

The user is provided with a keyfile 'key.pgp'.

## Solution

```
sq key import key.pgp
> Imported B790C58C1FF342445B10C8C3EA599D126ED52382 alice (UNAUTHENTICATED) from key.pgp: new
sq key list
> - softkeys
>  - B790C58C1FF342445B10C8C3EA599D126ED52382
>     - B790C58C1FF342445B10C8C3EA599D126ED52382 alice (UNAUTHENTICATED) (available, locked, for signing)
>     - 50C6AD0A3592FFCA4A31E9FAB7F994019FE190AF alice (UNAUTHENTICATED) (available, locked, for signing)
>     - 74839AD43B86D5B2DE04026D272AE67D4E899664 alice (UNAUTHENTICATED) (available, locked, for signing)
>     - A1A35E1F5B04B4A6D9CF690E8B67716DCF03D22D alice (UNAUTHENTICATED) (available, locked, for decryption)

```

