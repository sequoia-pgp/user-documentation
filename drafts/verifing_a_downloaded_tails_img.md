﻿### Verifing a downloaded tails image

The tails image comes with a detached signature - lets check if we got a legitimate image by verifing the signature.

Btw - `.local/share/pgp.cert.d` was emptied beforehand.

First download the image and signature, as described on the website [https://tails.net/install/expert/index.en.html] 

```
$ mkdir /tmp/workbench
$ cd /tmp/workbench
$ wget --continue https://download.tails.net/tails/stable/tails-amd64-6.1/tails-amd64-6.1.img
$ wget https://tails.net/torrents/files/tails-amd64-6.1.img.sig
$ ls -l
-rw-rw-r-- 1 malte malte 1433403392 Apr 15 11:28 tails-amd64-6.1.img
-rw-rw-r-- 1 malte malte        833 Apr 15 11:37 tails-amd64-6.1.img.sig

```

See what we got

```
$ sq inspect tails-amd64-6.1.img.sig 
tails-amd64-6.1.img.sig: Detached signature.

 Alleged signer: E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC
           Note: Signatures have NOT been verified!
```

> Neal: I've changed `sq inspect` to also include a label,
> when available.  See:
> https://gitlab.com/sequoia-pgp/sequoia-sq/-/issues/242
> The signer is actually the signing key's key ID, not the
> certificate's key ID.  If the certificate with that signing
> key is available locally,
> we could translate it to the certificate's fingerprint.
> I created this issue for further discussion:
> https://gitlab.com/sequoia-pgp/sequoia-sq/-/issues/243

Let's check the alleged signer

```
$ sq network fetch E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC
Note: Created a local CA to record provenance information.
Note: See `sq link list --ca` and `sq link --help` for more information.

Importing 13 certificates into the certificate store:

  1. 0109B6D7680FEEFDCE783EEE750AE75857622766 Tails APT repository
<tails@boum.org> (UNAUTHENTICATED)
  2. 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F Tails developers (Schleuder
mailing-list) <tails@boum.org> (UNAUTHENTICATED)
  3. 0D24B36AA9A2A651787876451202821CBE2CD9C1 Tails developers (signing key)
<tails@boum.org> (UNAUTHENTICATED)
  4. 2ED1191631B0B60CB27DA90A4C3DB83F3F50D2D9 gpg --check-sigs
A490D0F4D311A4153E2BB7CADBB802B258ACD84F <tails@boum.org> (UNAUTHENTICATED)
  5. 317026534CCC841EF92EB5D4F370463ECD68841B Tails developers <tails@boum.org>
(UNAUTHENTICATED)
  6. 3E97E609FC8DAE546FB6DF005C20D5DCAECAEF90 Tails developers <tails@boum.org>
(UNAUTHENTICATED)
  7. 98D080F83122CB4A3F45663825BF6AAECF914C38 Tails <tails@boum.org>
(UNAUTHENTICATED)
  8. 9BF8721D77F0242C8A6D0507D911B4A137BD2413 Tails developers (offline long-
term identity key) <tails@boum.org> (UNAUTHENTICATED)
  9. A490D0F4D311A4153E2BB7CADBB802B258ACD84F Tails developers (offline long-
term identity key) <tails@boum.org> (UNAUTHENTICATED)
  10. B6C645C871D595069ED1AE33DB1573A449F1F400 gpg --check-sigs
A490D0F4D311A4153E2BB7CADBB802B258ACD84F <tails@boum.org> (UNAUTHENTICATED)
  11. C3BAA4BFE369B2B86018B5150E08AC7806C069C8 BIGHEAD2 <TAILS@BOUM.ORG>
(UNAUTHENTICATED)
  12. D55A59A2CAFC5EAB7AA4777010C2D872BFBAF237 Tails <tails@boum.org>
(UNAUTHENTICATED)
  13. EB24960079A3E2B93BFE48B505F8BB78B38F4311 Amsesia <amnesia@boum.org>
(UNAUTHENTICATED)

Imported 13 new certificates, updated 0 certificates, 0 certificates unchanged,
0 errors.

After checking that a certificate really belongs to the stated owner, you can
mark the certificate as authenticated using:

    sq pki link add FINGERPRINT
```

Certificate #9 is the one mentioned on the tails website - looks good so far. But none of the enumerated certificates has the fingerprint `E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC`.

Just out of curiosity:

```
$ sq pki list E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC
There are no certificates with the specified fingerprint.  Run `sq network fetch
E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC` to look for matching certificates on
public directories.
Error: E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC was not found
```
What was it that `sq network fetch` actually fetched?

> Neal: The issue is that E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC
> is a subkey and sq pki list only allowed addressing certificates
> by the certificate's key ID or fingerprint.  I've changed this now:
> https://gitlab.com/sequoia-pgp/sequoia-sq/-/merge_requests/155
>
> ```
> $ sq pki list E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC --gossip
> Certificate A490D0F4D311A4153E2BB7CADBB802B258ACD84F contains the subkey E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC.
> A490D0F4D311A4153E2BB7CADBB802B258ACD84F will expire on 2025-01-25T09:24:54Z
>   [   0/120 ] Tails developers (offline long-term identity key) <tails@boum.org>
>   [   0/120 ] Tails developers <tails@boum.org>
> 
> To view why a user ID is considered valid, pass `--show-paths`
> 
> To see more details about a certificate, run:
> 
>   $ sq inspect --cert FINGERPRINT
> 
> After checking that a user ID really belongs to a certificate, use `sq pki link add` to mark
> the binding as authenticated, or use `sq network fetch FINGERPRINT|EMAIL` to look for new
> certifications.
> ```

Lets check the detached signature - it shouldn't authenticate yet, because there is no trust path.

```
$ sq verify --signer-cert E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC --detached tails-amd64-6.1.img.sig tails-amd64-6.1.img
Good signature from E5DBA2E186D5BAFC ("Tails developers (offline long-term
identity key) <tails@boum.org>")

1 good signature.
```

Eh - it does... why? Passing `--signer-cert` shortcuts the authentication - it says that for the invocation of this command we temporarily trust `E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC`.

> Neal: When you use `--signer-cert` you are effectively treating the
> specified certificate as a trust root.  The question you are asking
> is effectively: "Did one of these certificates make that signature?"
>
> If you have ideas of how to more clearly communicate these semantics,
> or if we should drop `--signer-cert` and require the caller to parse
> the output, or something else, please comment on this issue:
>
> https://gitlab.com/sequoia-pgp/sequoia-sq/-/issues/248

Some other thing: If we repeat the `sq network fetch`
```
$ sq network fetch E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC

Importing 13 certificates into the certificate store:

  1. 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F Tails developers (Schleuder
mailing-list) <tails@boum.org> (partially authenticated, 1/120)
  2. 0109B6D7680FEEFDCE783EEE750AE75857622766 Tails APT repository
<tails@boum.org> (UNAUTHENTICATED)
  3. 0D24B36AA9A2A651787876451202821CBE2CD9C1 Tails developers (signing key)
<tails@boum.org> (UNAUTHENTICATED)
  4. 2ED1191631B0B60CB27DA90A4C3DB83F3F50D2D9 gpg --check-sigs
A490D0F4D311A4153E2BB7CADBB802B258ACD84F <tails@boum.org> (UNAUTHENTICATED)
  5. 317026534CCC841EF92EB5D4F370463ECD68841B Tails developers <tails@boum.org>
(UNAUTHENTICATED)
  6. 3E97E609FC8DAE546FB6DF005C20D5DCAECAEF90 Tails developers <tails@boum.org>
(UNAUTHENTICATED)
  7. 98D080F83122CB4A3F45663825BF6AAECF914C38 Tails <tails@boum.org>
(UNAUTHENTICATED)
  8. 9BF8721D77F0242C8A6D0507D911B4A137BD2413 Tails developers (offline long-
term identity key) <tails@boum.org> (UNAUTHENTICATED)
  9. A490D0F4D311A4153E2BB7CADBB802B258ACD84F Tails developers (offline long-
term identity key) <tails@boum.org> (UNAUTHENTICATED)
  10. B6C645C871D595069ED1AE33DB1573A449F1F400 gpg --check-sigs
A490D0F4D311A4153E2BB7CADBB802B258ACD84F <tails@boum.org> (UNAUTHENTICATED)
  11. C3BAA4BFE369B2B86018B5150E08AC7806C069C8 BIGHEAD2 <TAILS@BOUM.ORG>
(UNAUTHENTICATED)
  12. D55A59A2CAFC5EAB7AA4777010C2D872BFBAF237 Tails <tails@boum.org>
(UNAUTHENTICATED)
  13. EB24960079A3E2B93BFE48B505F8BB78B38F4311 Amsesia <amnesia@boum.org>
(UNAUTHENTICATED)

Imported 0 new certificates, updated 0 certificates, 13 certificates unchanged,
0 errors.

After checking that a certificate really belongs to the stated owner, you can
mark the certificate as authenticated using:

    sq pki link add FINGERPRINT
```

The first certificate now is partially authenticated. Lets investigate that.


```
$ sq pki list
[ ] 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F Tails developers (Schleuder
    mailing-list) <tails@boum.org>: marginally authenticated (0%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   partially certified (amount: 40 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (40 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 1C82C062041374CB434FDDC8DB4EE850A95A8934 ("Public Directories")
  │   partially certified (amount: 1 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (1 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 12BA0E3EB14335A76C4CF8104339F3746BB62308 ("Downloaded from
  │ keys.openpgp.org")
  │   certified the following binding on 2024-04-15
  └ 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F "Tails developers (Schleuder
    mailing-list) <tails@boum.org>"

[ ] 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F Tails list (schleuder list) <tails-
    owner@boum.org>: marginally authenticated (0%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   partially certified (amount: 40 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (40 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 1C82C062041374CB434FDDC8DB4EE850A95A8934 ("Public Directories")
  │   partially certified (amount: 1 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (1 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 12BA0E3EB14335A76C4CF8104339F3746BB62308 ("Downloaded from
  │ keys.openpgp.org")
  │   certified the following binding on 2024-04-15
  └ 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F "Tails list (schleuder list)
    <tails-owner@boum.org>"

[ ] 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F Tails list (schleuder list) <tails-
    request@boum.org>: marginally authenticated (0%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   partially certified (amount: 40 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (40 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 1C82C062041374CB434FDDC8DB4EE850A95A8934 ("Public Directories")
  │   partially certified (amount: 1 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (1 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 12BA0E3EB14335A76C4CF8104339F3746BB62308 ("Downloaded from
  │ keys.openpgp.org")
  │   certified the following binding on 2024-04-15
  └ 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F "Tails list (schleuder list)
    <tails-request@boum.org>"

[ ] 12BA0E3EB14335A76C4CF8104339F3746BB62308 Downloaded from keys.openpgp.org:
    marginally authenticated (0%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   partially certified (amount: 40 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (40 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 1C82C062041374CB434FDDC8DB4EE850A95A8934 ("Public Directories")
  │   partially certified (amount: 1 of 120) the following binding on 2002-02-20
  │   as a partially trusted (1 of 120) meta-introducer (depth: unconstrained)
  └ 12BA0E3EB14335A76C4CF8104339F3746BB62308 "Downloaded from keys.openpgp.org"

[ ] 1C82C062041374CB434FDDC8DB4EE850A95A8934 Public Directories: partially
    authenticated (33%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   partially certified (amount: 40 of 120) the following binding on 2002-
  │   02-20 as a partially trusted (40 of 120) meta-introducer (depth:
  │   unconstrained)
  └ 1C82C062041374CB434FDDC8DB4EE850A95A8934 "Public Directories"

[✓] C746308CCC319B78998247210B986BAF950E297A Local Trust Root: fully
    authenticated (100%)
  ◯ C746308CCC319B78998247210B986BAF950E297A "Local Trust Root"

18 bindings found.
Skipped 17 bindings, which could not be authenticated.
Pass `--gossip` to see the unauthenticated bindings.
```

Our understanding would be that `E1693B1DAB525F2AAD6702DCE5DBA2E186D5BAFC` was downloaded from `keys.openpgp.org` and that server was granted a 40 out of 120 "trust level" to introduce new certificates. But it's not authenticated.

Is there any way to modify this "40 out of 120" setting? Maybe we already trust keys.openpgp.org enough.

> Neal: Good question.  First, the trust chain is:
>
> Local trust root -> Public Directories -> `keys.openpgp.org` -> certificate
>
> Public Directories is an intermediate CA, which acts as a resistor:
> all of the public directories together can't authenticate the certificate
> more than 40/120.
>
> By default, Public Directories says that `keys.openpgp.org` is an intermediate
> CA that is trusted 1/120.
>
> To consider `keys.openpgp.org` as a fully trusted CA, you can do:
>
> ```
> $ sq pki link add --ca \* 83479ED298A650846F39654A522DC7CD1531DC69 --all
> Linking 83479ED298A650846F39654A522DC7CD1531DC69 and "Downloaded from keys.openpgp.org".
> ```
>
> Note: 83479ED298A650846F39654A522DC7CD1531DC69 is my shadow CA for koo.
> You'd use 12BA0E3EB14335A76C4CF8104339F3746BB62308.
>
> How to symbolicly name intermediate CAs is tracked in this issue:
> https://gitlab.com/sequoia-pgp/sequoia-sq/-/issues/206
>
> Comments welcome!

Lets nail that down:

```
$ sq pki list 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F --certification-network
[ ] 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F Tails developers (Schleuder
    mailing-list) <tails@boum.org>: marginally authenticated (0%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   partially certified (amount: 40 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (40 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 1C82C062041374CB434FDDC8DB4EE850A95A8934 ("Public Directories")
  │   partially certified (amount: 1 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (1 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 12BA0E3EB14335A76C4CF8104339F3746BB62308 ("Downloaded from
  │ keys.openpgp.org")
  │   certified the following certificate on 2024-04-15
  ├ 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F ("Amnesia <amnesia@boum.org>")
  │   certified the following binding on 2024-04-15
  └ 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F "Tails developers (Schleuder
    mailing-list) <tails@boum.org>"

[ ] 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F Tails list (schleuder list) <tails-
    owner@boum.org>: marginally authenticated (0%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   partially certified (amount: 40 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (40 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 1C82C062041374CB434FDDC8DB4EE850A95A8934 ("Public Directories")
  │   partially certified (amount: 1 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (1 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 12BA0E3EB14335A76C4CF8104339F3746BB62308 ("Downloaded from
  │ keys.openpgp.org")
  │   certified the following certificate on 2024-04-15
  ├ 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F ("Amnesia <amnesia@boum.org>")
  │   certified the following binding on 2024-04-15
  └ 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F "Tails list (schleuder list)
    <tails-owner@boum.org>"

[ ] 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F Tails list (schleuder list) <tails-
    request@boum.org>: marginally authenticated (0%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   partially certified (amount: 40 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (40 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 1C82C062041374CB434FDDC8DB4EE850A95A8934 ("Public Directories")
  │   partially certified (amount: 1 of 120) the following certificate on
  │   2002-02-20 as a partially trusted (1 of 120) meta-introducer (depth:
  │   unconstrained)
  ├ 12BA0E3EB14335A76C4CF8104339F3746BB62308 ("Downloaded from
  │ keys.openpgp.org")
  │   certified the following binding on 2024-04-15
  └ 09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F "Tails list (schleuder list)
    <tails-request@boum.org>"

5 bindings found.
Skipped 5 bindings, which could not be authenticated.
Pass `--gossip` to see the unauthenticated bindings.
Error: Could not authenticate any paths.
```

> Neal: It's unclear to me why you are using `--certification-network`.  What was your motivation?

This somehow conflicts with the previous statement that `09F6BC8FEEC9D8EE005DBAA41D2975EDF93E735F` was partially authenticated. Maybe this is just a wording issue: 'partially authenticated' != 'authenticated'.

> Neal: I'm confused.  I don't think the above output says that it is authenticate?  It says "marginally authenticated" (which is indeed a shift in terminology)

But continue with the verification of the tails signature. Lets do without `--signer-cert`

```
$ sq verify --detached tails-amd64-6.1.img.sig tails-amd64-6.1.img
Authenticating A490D0F4D311A4153E2BB7CADBB802B258ACD84F ("Tails developers
(offline long-term identity key) <tails@boum.org>") using the web of trust:
  A490D0F4D311A4153E2BB7CADBB802B258ACD84F: "Tails developers (offline long-term
identity key) <tails@boum.org>" is unauthenticated and may be an impersonation!
  A490D0F4D311A4153E2BB7CADBB802B258ACD84F: "Tails developers <tails@boum.org>"
is unauthenticated and may be an impersonation!
  Unauthenticated checksum from E5DBA2E186D5BAFC ("Tails developers (offline
long-term identity key) <tails@boum.org>")
    After checking that E5DBA2E186D5BAFC belongs to "Tails developers (offline
long-term identity key) <tails@boum.org>", you can authenticate the binding
using 'sq link add E5DBA2E186D5BAFC "Tails developers (offline long-term
identity key) <tails@boum.org>"'.

1 unauthenticated checksum.
Error: Verification failed: could not fully authenticate any signatures
```

Thats no suprise.


## The Tails way

Tails suggests to import its signing key and the key of Chris Lamb from the debian keyring. The idea is that there is trust in the debian developers and Chris is one of them and he signed the tails signing key.

```
$ wget https://tails.net/tails-signing.key
$ sq cert import tails-signing.key 
Imported A490D0F4D311A4153E2BB7CADBB802B258ACD84F, Tails developers (offline
long-term identity key) <tails@boum.org> (UNAUTHENTICATED)
Imported 0 new certificates, updated 0 certificates, 1 certificates unchanged,
0 errors.
```
It's already there - this is somewhat expected.

```
$ sq inspect --cert A490D0F4D311A4153E2BB7CADBB802B258ACD84F --certifications | grep "Alleged certifier:" | wc -l

WARNING: sq does not have a stable CLI interface. Use with caution in scripts.

2161
```

2161 certifications. Since we started with a clean slate, we currently know nothing about them.

Lets look at Chris certificate

```
$ gpg --keyring=/usr/share/keyrings/debian-keyring.gpg --export chris@chris-lamb.co.uk | sq inspect
-: OpenPGP Certificate.

    Fingerprint: C2FE4BD271C139B86C533E461E953E27D4311E58
Public-key algo: RSA
Public-key size: 4096 bits
  Creation time: 2009-07-12 19:43:47 UTC
      Key flags: certification, signing

         Subkey: 0E08C16E029D46527FA2FB4C72B3DBA98575B3F2
Public-key algo: RSA
Public-key size: 4096 bits
  Creation time: 2009-07-12 19:49:01 UTC
      Key flags: transport encryption, data-at-rest encryption

         UserID: Chris Lamb <chris@chris-lamb.co.uk>
 Certifications: 28, use --certifications to list

         UserID: Chris Lamb <lamby@debian.org>
 Certifications: 31, use --certifications to list

         UserID: Chris Lamb <lamby@gnu.org>
 Certifications: 29, use --certifications to list
```

There is a fingerprint... lets see if it is one of the alleged signers.

```
$ sq inspect --cert A490D0F4D311A4153E2BB7CADBB802B258ACD84F --certifications | grep C2FE4BD271C139B86C533E461E953E27D4311E58

WARNING: sq does not have a stable CLI interface. Use with caution in scripts.

                 Alleged certifier: C2FE4BD271C139B86C533E461E953E27D4311E58
                 Alleged certifier: C2FE4BD271C139B86C533E461E953E27D4311E58
```
AHA!

Lets import Chris certificate
```
$ gpg --keyring=/usr/share/keyrings/debian-keyring.gpg --export chris@chris-lamb.co.uk | sq cert import
Imported C2FE4BD271C139B86C533E461E953E27D4311E58, Chris Lamb <chris@chris-
lamb.co.uk> (UNAUTHENTICATED)
Imported 1 new certificates, updated 0 certificates, 0 certificates unchanged,
0 errors.
```

Add trust to that certificate

```
$ sq pki link add C2FE4BD271C139B86C533E461E953E27D4311E58 --all
Linking C2FE4BD271C139B86C533E461E953E27D4311E58 and "Chris Lamb <chris@chris-
lamb.co.uk>".

Linking C2FE4BD271C139B86C533E461E953E27D4311E58 and "Chris Lamb
<lamby@debian.org>".

Linking C2FE4BD271C139B86C533E461E953E27D4311E58 and "Chris Lamb
<lamby@gnu.org>".
```

Lets see if something changed

```
$ sq verify --detached tails-amd64-6.1.img.sig tails-amd64-6.1.img
Authenticating A490D0F4D311A4153E2BB7CADBB802B258ACD84F ("Tails developers
(offline long-term identity key) <tails@boum.org>") using the web of trust:
  A490D0F4D311A4153E2BB7CADBB802B258ACD84F: "Tails developers (offline long-term
identity key) <tails@boum.org>" is unauthenticated and may be an impersonation!
  A490D0F4D311A4153E2BB7CADBB802B258ACD84F: "Tails developers <tails@boum.org>"
is unauthenticated and may be an impersonation!
  Unauthenticated checksum from E5DBA2E186D5BAFC ("Tails developers (offline
long-term identity key) <tails@boum.org>")
    After checking that E5DBA2E186D5BAFC belongs to "Tails developers (offline
long-term identity key) <tails@boum.org>", you can authenticate the binding
using 'sq link add E5DBA2E186D5BAFC "Tails developers (offline long-term
identity key) <tails@boum.org>"'.

1 unauthenticated checksum.
Error: Verification failed: could not fully authenticate any signatures
```

Not yet... it's not entirely clear what is missing.

> Neal: So what happened is that you said the bindings "C2FE4BD271C139B86C533E461E953E27D4311E58 and "Chris Lamb <chris@chris-lamb.co.uk>" etc., are authenticated.  But, you didn't say that the certificate should be used as a trusted introducer, i.e., a CA.
>
> If you want to mark Chris as an unrestricted CA, you should do:
>
> ```
> $ sq pki link add --ca \* C2FE4BD271C139B86C533E461E953E27D4311E58 --all
> ```
>
> That's perhaps not the best idea, because then Chris can authenticate any binding.  Instead, we can restrict Chris to just user IDs that have an email address in `boum.org` as follows:
>
> ```
> $ sq pki link add --ca boum.org C2FE4BD271C139B86C533E461E953E27D4311E58 --all
> ```
>
> Even better would be to allow only authenticating a particular email address or
> user ID.  (OpenPGP supports this, but GnuPG doesn't.)
>
> https://gitlab.com/sequoia-pgp/sequoia-sq/-/issues/249



```
$ sq pki authenticate A490D0F4D311A4153E2BB7CADBB802B258ACD84F --email tails@boum.org --gossip
[ ] A490D0F4D311A4153E2BB7CADBB802B258ACD84F Tails developers (offline long-term
    identity key) <tails@boum.org>: not authenticated (0%)
  ◯ 0D24B36AA9A2A651787876451202821CBE2CD9C1 ("T(A)ILS developers (signing key)
  <amnesia@boum.org>")
  │   certified the following binding on 2015-01-19
  └ A490D0F4D311A4153E2BB7CADBB802B258ACD84F "Tails developers (offline long-
    term identity key) <tails@boum.org>"

[ ] A490D0F4D311A4153E2BB7CADBB802B258ACD84F Tails developers (offline long-term
    identity key) <tails@boum.org>: not authenticated (0%)
  ◯ A490D0F4D311A4153E2BB7CADBB802B258ACD84F ("Tails developers (offline long-
  term identity key) <tails@boum.org>")
  │   certified the following binding on 2023-10-03
  └ A490D0F4D311A4153E2BB7CADBB802B258ACD84F "Tails developers (offline long-
    term identity key) <tails@boum.org>"

[ ] A490D0F4D311A4153E2BB7CADBB802B258ACD84F Tails developers (offline long-term
    identity key) <tails@boum.org>: not authenticated (0%)
  ◯ C2FE4BD271C139B86C533E461E953E27D4311E58 ("Chris Lamb <chris@chris-
  lamb.co.uk>")
  │   certified the following binding on 2020-03-19
  └ A490D0F4D311A4153E2BB7CADBB802B258ACD84F "Tails developers (offline long-
    term identity key) <tails@boum.org>"

[ ] A490D0F4D311A4153E2BB7CADBB802B258ACD84F Tails developers <tails@boum.org>:
    not authenticated (0%)
  ◯ C2FE4BD271C139B86C533E461E953E27D4311E58 ("Chris Lamb <chris@chris-
  lamb.co.uk>")
  │   certified the following binding on 2020-03-19
  └ A490D0F4D311A4153E2BB7CADBB802B258ACD84F "Tails developers <tails@boum.org>"

[ ] A490D0F4D311A4153E2BB7CADBB802B258ACD84F Tails developers <tails@boum.org>:
    not authenticated (0%) 
  ◯ A490D0F4D311A4153E2BB7CADBB802B258ACD84F ("Tails developers (offline long-
  term identity key) <tails@boum.org>")
  │   certified the following binding on 2023-10-03
  └ A490D0F4D311A4153E2BB7CADBB802B258ACD84F "Tails developers <tails@boum.org>"

After checking that a user ID really belongs to a certificate, use `sq pki link
add` to mark the binding as authenticated, or use `sq network fetch FINGERPRINT|
EMAIL` to look for new certifications.

```

Chris certification appears. But that doesn't seem to be enough to actually authenticate `A490D0F4D311A4153E2BB7CADBB802B258ACD84F`.

```
$ sq pki list C2FE4BD271C139B86C533E461E953E27D4311E58
[✓] C2FE4BD271C139B86C533E461E953E27D4311E58 Chris Lamb <chris@chris-
    lamb.co.uk>: fully authenticated (100%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   certified the following binding on 2024-04-15
  └ C2FE4BD271C139B86C533E461E953E27D4311E58 "Chris Lamb <chris@chris-
    lamb.co.uk>"

[✓] C2FE4BD271C139B86C533E461E953E27D4311E58 Chris Lamb <lamby@debian.org>:
    fully authenticated (100%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   certified the following binding on 2024-04-15
  └ C2FE4BD271C139B86C533E461E953E27D4311E58 "Chris Lamb <lamby@debian.org>"

[✓] C2FE4BD271C139B86C533E461E953E27D4311E58 Chris Lamb <lamby@gnu.org>: fully
    authenticated (100%)
  ◯ C746308CCC319B78998247210B986BAF950E297A ("Local Trust Root")
  │   certified the following binding on 2024-04-15
  └ C2FE4BD271C139B86C533E461E953E27D4311E58 "Chris Lamb <lamby@gnu.org>"
```

Do we somehow have to promote `C2FE4BD271C139B86C533E461E953E27D4311E58` to be a (intermediate) CA?

> Neal: Yes!  (See the comment above.)

We are a bit lost!
