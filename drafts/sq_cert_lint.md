﻿
#### sq cert lint

A certification signature is a way to express trust in the authenticity of a binding between a User ID (like an email address) and a public key. The certification signature is added to the certificate containing the binding. This augmented certificate can then be published, allowing other users to see and consider this expression of trust.

> Neal: An aside: in Mark Miller's Robust Composition thesis (http://www.erights.org/talks/thesis/markm-thesis.pdf), he talks about the different between "trust" and "reliance" and argues that the latter is usually a better, less ambiguous term:
>
>>  “Rely” is deﬁned in terms of the objective situation (P is vulnerable to R), and so avoids confusions engendered by the word “trust.” (page 30, footnote 1)
>
> Relatedly Norm Hardy defines [Reliance set](http://cap-lore.com/CapTheory/Glossary.html) as follows:
>
>> When we say that a capability platform has some property, it is always relative to some set of objects whose defining code must perform as designed. The reliance set is that set of code. The reliance set is about the same as a TCB but depends on what property is under discussion. Any application generally relies for its security on other software on the platform. It relies on a larger set for its correctness. Most platforms have some bottom software called a kernel which is in every reliance set. 
>
> As trust is such an overloaded term, I also try to avoid it in particular as regards third parties or artifacts issued by them.  So rather than say, "Alice trusts Foo's CA," I try to say "Alice relies on Foo's CA."  My feeling is that this makes the "making oneself vulnerable" a bit clearer.
>
> Whether one uses "trust" or "rely" though, making clear *what* is being trusted is essential.  So: "Alice trusts/relies on Foo's CA to authenticate the certificates for Foo's members/employees."
>
> In the above, I'd actually talk about "assertions" rather than "trust".  So:
>
>> A certification signature is a way to assert the authenticity of a binding... to see and consider this assertion.


The first certification signature is added to a key when it's generated. It selfsigns the key creating a binding between it's public key and it's initial User IDs. Revocation certificates also contain a certification signature to verify the authenticity of the revocation.

The certification signature is created by hashing the public key and the User ID and subsequently signing the resulting hash value. Ideally the hash value for the combination of public key and User ID should be unique. Hash values however are of limited size which implies an information loss – there exist other input values which produce the same hash value. The consequence of these collisions is that a certification signature expresses trust in an unlimited amount of public key - User ID pairs. Obviously this is not intended.

One design criteria for a hash function used for the creation of certification signatures is, that these collisions are next to impossible to construct making the abuse of a collision infeasible in practice.

SHA1 is one of such hash functions. It turned out that with the expanding availability of computing power SHA1 became vulnerable, meaning the construction of a fitting public key – User ID pair for a hash value became a realistic scenario. The remedy is to replace a SHA1 hash with a hash calculated by a stronger hash function: SHA256. 
  

> Neal: Actually, the additional computing power was not the primary reason SHA-1 is considered broken: researchers found correlations between different bits in subsequent rounds in SHA-1.  These were exploited to reduce the strength of SHA-1 from 80 bits of collision resistance to about 63 bits (IIRC).


### Let's check for SHA1 signatures in certificates and replace them with SHA256 signatures

We use sq cert lint to check for certificates that use SHA-1 signatures (like User ID's self signature, a subkeys's current binding signature, a signing-capable subkey's current backsig or revocations) and, if any were found, fix them.

First, having a look at our public keyring to find out which certificates we trust that contain SHA-1-protected User ID's or keys.


```
$ gpg --homedir=/tmp/workbench/ --export --armour | sq cert lint
gpg: WARNING: unsafe permissions on homedir '/tmp/workbench'
Certificate 27E8C659C86F1C42 is not valid under the standard policy: No binding signature at time 2024-04-18T09:26:21Z
Certificate 27E8C659C86F1C42 contains a User ID ("Bob Example (GnuPG 1.4.18 Testkey) <bob@example.com>") protected by SHA-1
Certificate 27E8C659C86F1C42, key 739247043F705BAD uses a SHA-1-protected binding signature.
Certificate 9C2437DF50A1F904 is not valid under the standard policy: No binding signature at time 2024-04-18T09:26:21Z
Certificate 9C2437DF50A1F904 contains a User ID ("Alice Example (GnuPG 1.4.18 Testkey) <alice@example.com>") protected by SHA-1
Certificate 9C2437DF50A1F904, key 6FBFA7A845431489 uses a SHA-1-protected binding signature.
Examined 4 certificates.
  0 certificates are invalid and were not linted. (GOOD)
  4 certificates were linted.
  2 of the 4 certificates (50%) have at least one issue. (BAD)
0 of the linted certificates were revoked.
  0 of the 0 certificates has revocation certificates that are weaker than the certificate and should be recreated. (GOOD)
0 of the linted certificates were expired.
4 of the non-revoked linted certificates have at least one non-revoked User ID:
  2 have at least one User ID protected by SHA-1. (BAD)
  2 have all User IDs protected by SHA-1. (BAD)
4 of the non-revoked linted certificates have at least one non-revoked, live subkey:
  2 have at least one non-revoked, live subkey with a binding signature that uses SHA-1. (BAD)
0 of the non-revoked linted certificates have at least one non-revoked, live, signing-capable subkey:
  0 certificates have at least one non-revoked, live, signing-capable subkey with a strong binding signature, but a backsig that uses SHA-1. (GOOD)
```


or with --list-keys:

```
$ gpg --homedir=/tmp/workbench/ --export --armour | sq cert lint --list-keys
gpg: WARNING: unsafe permissions on homedir '/tmp/workbench'
F39D47AD38E2FC90D86FB94B27E8C659C86F1C42
000D6166AEC5E2EDA801BC259C2437DF50A1F904
Examined 4 certificates.
```

That's not looking too good. So what about checking our own cert?


```
$ gpg --export bob@example.com | sq cert lint
Certificate 27E8C659C86F1C42 is not valid under the standard policy: No binding signature at time 2024-04-17T12:10:44Z
Certificate 27E8C659C86F1C42 contains a User ID ("Bob Example (GnuPG 1.4.18 Testkey) <bob@example.com>") protected by SHA-1
Certificate 27E8C659C86F1C42, key 739247043F705BAD uses a SHA-1-protected binding signature.
Examined 1 certificate.
  0 certificates are invalid and were not linted. (GOOD)
  1 certificate was linted.
  1 of the 1 certificates (100%) has at least one issue. (BAD)
0 of the linted certificates were revoked.
  0 of the 0 certificates has revocation certificates that are weaker than the certificate and should be recreated. (GOOD)
0 of the linted certificates were expired.
1 of the non-revoked linted certificate has at least one non-revoked User ID:
  1 has at least one User ID protected by SHA-1. (BAD)
  1 has all User IDs protected by SHA-1. (BAD)
1 of the non-revoked linted certificates has at least one non-revoked, live subkey:
  1 has at least one non-revoked, live subkey with a binding signature that uses SHA-1. (BAD)
0 of the non-revoked linted certificates have at least one non-revoked, live, signing-capable subkey:
  0 certificates have at least one non-revoked, live, signing-capable subkey with a strong binding signature, but a backsig that uses SHA-1. (GOOD)
```


There's one SHA1 signature found, so we want to fix and import it to the gnupg keyring:

```
  $ gpg --export-secret-key bob@example.com | sq cert lint --fix | gpg --import
Certificate 27E8C659C86F1C42 is not valid under the standard policy: No binding signature at time 2024-04-17T12:20:50Z
Certificate 27E8C659C86F1C42 contains a User ID ("Bob Example (GnuPG 1.4.18 Testkey) <bob@example.com>") protected by SHA-1
Certificate 27E8C659C86F1C42, key 739247043F705BAD uses a SHA-1-protected binding signature.
Examined 1 certificate.
  0 certificates are invalid and were not linted. (GOOD)
  1 certificate was linted.
  1 of the 1 certificates (100%) has at least one issue. (BAD)
0 of the linted certificates were revoked.
  0 of the 0 certificates has revocation certificates that are weaker than the certificate and should be recreated. (GOOD)
0 of the linted certificates were expired.
1 of the non-revoked linted certificate has at least one non-revoked User ID:
  1 has at least one User ID protected by SHA-1. (BAD)
  1 has all User IDs protected by SHA-1. (BAD)
1 of the non-revoked linted certificates has at least one non-revoked, live subkey:
  1 has at least one non-revoked, live subkey with a binding signature that uses SHA-1. (BAD)
0 of the non-revoked linted certificates have at least one non-revoked, live, signing-capable subkey:
  0 certificates have at least one non-revoked, live, signing-capable subkey with a strong binding signature, but a backsig that uses SHA-1. (GOOD)
gpg: key 27E8C659C86F1C42: "Bob Example (GnuPG 1.4.18 Testkey) <bob@example.com>" 2 new signatures
gpg: Total number processed: 1
gpg:         new signatures: 2
```



Okay, the output looks similar to the output above. We now have a look at the cert again, expecting it to be linted:


```
$ gpg --export bob@example.com | sq cert lint

NO OUTPUT
```



Does it mean it is fixed already?  

-> If sq cert lint doesn't find a SHA-1 signature it maybe is a good idea to adjust the output and write ' no SHA-1 signatures found' instead of having no output at all?

> Neal: This command used to show something when there were no issues, but people requested that the linter be quiet if there were no issues detected.  See this issue for more details: https://gitlab.com/sequoia-pgp/keyring-linter/-/issues/4


We try to examine the signatures with sq inspect:


```
$ gpg --export bob@example.com | sq inspect
-: OpenPGP Certificate.

    Fingerprint: F39D47AD38E2FC90D86FB94B27E8C659C86F1C42
Public-key algo: RSA
Public-key size: 2048 bits
  Creation time: 2024-04-17 09:46:37 UTC
Expiration time: 2025-04-17 09:46:37 UTC (creation time + 11months 30days 3h 50m 24s)
      Key flags: certification, signing

         Subkey: 509105DD8BB39C3925F939BE739247043F705BAD
Public-key algo: RSA
Public-key size: 2048 bits
  Creation time: 2024-04-17 09:46:37 UTC
Expiration time: 2025-04-17 09:46:37 UTC (creation time + 11months 30days 3h 50m 24s)
      Key flags: transport encryption, data-at-rest encryption

         UserID: Bob Example (GnuPG 1.4.18 Testkey) <bob@example.com>
```



We expected to see a reference to the hash function, so we try sq toolbox packet dump for more information:


```
gpg --export bob@example.com | sq toolbox packet dump
Public-Key Packet, old CTB, 269 bytes
    Version: 4
    Creation time: 2024-04-17 09:46:37 UTC
    Pk algo: RSA
    Pk size: 2048 bits
    Fingerprint: F39D47AD38E2FC90D86FB94B27E8C659C86F1C42
    KeyID: 27E8C659C86F1C42
  
User ID Packet, old CTB, 52 bytes
    Value: Bob Example (GnuPG 1.4.18 Testkey) <bob@example.com>
  
Signature Packet, old CTB, 318 bytes
    Version: 4
    Type: PositiveCertification
    Pk algo: RSA
    Hash algo: SHA1
    Hashed area:
      Signature creation time: 2024-04-17 09:46:37 UTC
      Key flags: CS
      Key expiration time: 11months 30days 3h 50m 24s
      Symmetric algo preferences: AES256, AES192, AES128, CAST5, TripleDES
      Hash preferences: SHA256, SHA1, SHA384, SHA512, SHA224
      Compression preferences: Zlib, BZip2, Zip
      Features: SEIPDv1
      Keyserver preferences: no modify
    Unhashed area:
      Issuer: 27E8C659C86F1C42
    Digest prefix: 23A9
    Level: 0 (signature over data)
  
Signature Packet, old CTB, 408 bytes
    Version: 4
    Type: PositiveCertification
    Pk algo: RSA
    Hash algo: SHA256
    Hashed area:
      Signature creation time: 2024-04-17 12:20:50 UTC (critical)
      Key expiration time: 11months 30days 3h 50m 24s
      Symmetric algo preferences: AES256, AES192, AES128
      Issuer: 27E8C659C86F1C42
      Notation: salt@notations.sequoia-pgp.org
        00000000  81 e7 68 6d aa e8 67 8e  68 f9 15 96 fa 79 f1 2a
        00000010  51 54 e4 9a b8 2a 82 4d  0d 47 88 72 e9 44 45 12
      Hash preferences: SHA256, SHA512
      Compression preferences: Zlib, BZip2, Zip
      Keyserver preferences: no modify
      Key flags: CS
      Features: SEIPDv1
      Issuer Fingerprint: F39D47AD38E2FC90D86FB94B27E8C659C86F1C42
    Digest prefix: 9A8C
    Level: 0 (signature over data)
  
Public-Subkey Packet, old CTB, 269 bytes
    Version: 4
    Creation time: 2024-04-17 09:46:37 UTC
    Pk algo: RSA
    Pk size: 2048 bits
    Fingerprint: 509105DD8BB39C3925F939BE739247043F705BAD
    KeyID: 739247043F705BAD
  
Signature Packet, old CTB, 293 bytes
    Version: 4
    Type: SubkeyBinding
    Pk algo: RSA
    Hash algo: SHA1
    Hashed area:
      Signature creation time: 2024-04-17 09:46:37 UTC
      Key flags: EtEr
      Key expiration time: 11months 30days 3h 50m 24s
    Unhashed area:
      Issuer: 27E8C659C86F1C42
    Digest prefix: 882A
    Level: 0 (signature over data)
  
Signature Packet, old CTB, 388 bytes
    Version: 4
    Type: SubkeyBinding
    Pk algo: RSA
    Hash algo: SHA256
    Hashed area:
      Signature creation time: 2024-04-17 12:20:50 UTC (critical)
      Key expiration time: 11months 30days 3h 50m 24s
      Issuer: 27E8C659C86F1C42
      Notation: salt@notations.sequoia-pgp.org
        00000000  b1 73 8c 9b 5c f2 0b d9  69 d7 cb 67 46 b1 ef bb
        00000010  c4 1c 88 23 98 36 06 50  a9 5d ca 35 26 b5 b8 a7
      Key flags: EtEr
      Issuer Fingerprint: F39D47AD38E2FC90D86FB94B27E8C659C86F1C42
    Digest prefix: 5381
    Level: 0 (signature over data)
```



## Fixing a revocation certificate

First, lets look into a revocation certificate to examine the hash algorithm used (bob_rev.pgp contains the certificate):

```
$ sq toolbox packet dump  bob_rev.pgp
Signature Packet, old CTB, 302 bytes
    Version: 4
    Type: KeyRevocation
    Pk algo: RSA
    Hash algo: SHA1
    Hashed area:
      Signature creation time: 2024-04-18 09:48:29 UTC
      Reason for revocation: Key is retired and no longer used, Test revocation
    Unhashed area:
      Issuer: 27E8C659C86F1C42
    Digest prefix: 18DB
    Level: 0 (signature over data)
```
`Hash algo` tells the used algorithm, SHA1 in this case.

`sq cert lint` is not meant to fix this issue. Instead simply create a new revocation certificate:

TBD...

