## Acknowledgments

Sequoia PGP is an implementation of the [OpenPGP](https://datatracker.ietf.org/wg/openpgp/documents/) standard. As such it is a successor of the venerable PGP, developed by Phil Zimmermann, reflecting his commitment to human rights and the peace movement. Our first thanks goes to Phil. PGP provided strong encryption for everyone, kick starting a ecosystem of privacy protecting software many have made contributions to. Thanks go out to them as well.

### Team

Of course, this book would not exist without the Sequoia team. That's why we would like to thank the main developers Neal H. Walfield and Justus Winter, as well as the current team members Devan, the projects devop, Fabio (decthorpe), the Fedora Linux 'sq' package maintainer, Holger Levsen, Debian package maintainer for 'sq' together with dkg and Alexander Kjäll.

In addition, there are many other people who contributed to the project or have done so in the past. Some of them can be found [here](https://sequoia-pgp.org/community).

### Documentation

This documentation was written by Franziska Schmidtke and Malte Meiboom. 

### Funding

The [Sovereign Tech Fund](https://www.sovereign.tech/) funded the project in 2024 - without them, this documentation would probably not exist. 
