## Rotating a certificate

If you want to replace a certificate with a new one and take over the capabilities, you can use `sq key rotate`.
The new certificate will then contain the same self-signed User IDs and both, the old and the new one, will cross certify each other as unconstrained trust introducers. Also the certifications that where made by the old certificate will be replayed by the new cert.

The new certificate will be marked in the same way as the old one as 'own key' or 'shared key'. Per default, its expiration will be set to three years. If the old primary key was marked to have certain capabilities, this will not be recreated for the new primary key in any case. Instead separate subkeys will be assigned to these capabilities. Only the ability to issue certifications will be transferred to the new primary key. These settings are all adjustable.

One more important thing is the replacement date of the old certificate. There will be generated a revocation certificate which indicates the retirement of the old certificate in defaultly 182 days. The idea is to have a decent amount of time for the transition period. Of course this is also changeable, as shown in the example below.


Here you can see the result of applying the command on an existing certificate. First, we have a look into it:



```shell
$ sq cert list 740927F8A0188AA02FCE4B672D8E4019B47A6891
 - 740927F8A0188AA02FCE4B672D8E4019B47A6891
   - created 2024-12-16 16:07:20 UTC
   - will expire 2027-12-17T09:33:41Z

   - [    ✓    ] <alice@example.com>
[...]
```


Now we apply the rotation command, using a supershort retire period to show the results immediately:


```shell
$ sq key rotate --cert 740927F8A0188AA02FCE4B672D8E4019B47A6891 --retire-in 1s
Please enter the password to protect the new key (press enter to not use a password): 
                                                          Please repeat the password: 

Cross signing the old and new certificates.

Replaying the old certificate's links:
  Copying link for <alice@example.com>:
   - created at 2024‑12‑16 16:07:20
   - linked as a fully trusted CA

Replaying certifications made by the old certificate:
  Considering the source certificate's certification of the binding:
   - ┌ 1C88780EF239586AF463758094119887B3462B88
     └ <bob@example.com>
    Source certificate's active certification:
     - created at 2024‑12‑16 16:25:59
     - certified as a partially trusted CA
     - expiration: 2034‑12‑17
     - trust depth: 1
     - regular expressions: <[^>]+[@.]example\.com>$
    Replayed certification.

Retiring the old certificate as of 2025‑02‑06 13:21:15.

Transferable Secret Key.
[...]
```



If we now have a look at the original certificate again, we see that is is revoked yet: 


```shell
$ sq cert list 740927F8A0188AA02FCE4B672D8E4019B47A6891
No bindings matching "740927F8A0188AA02FCE4B672D8E4019B47A6891" could be authenticated.
  - Warning: 740927F8A0188AA02FCE4B672D8E4019B47A6891 is revoked: Key is retired and no longer usedThis certificate has been retired, and is replaced by
    03D91FD4DDEE0B5A4AECC52CA0950246DF806DAF..
1 binding found.
Skipped 1 binding, which is unusable.

  Error: No bindings matching the query could be authenticated.
```

### Replaying certifications

One part of operations of `sq key rotate` is (re)creating all the certifications made by the old key using the new one. This operation can be done on its own by:

```shell
$ sq pki vouch replay --source $SOURCE_FPR --target $TARGET_FPR
```

This command takes all certifications made with `$SOURCE_FPR` and certifies them using `$TARGET_FPR`. Both, source and target can be designated by 

- fingerprint (as shown above)
- email: `--source-email`, `--target-email`
- userid: `--source-userid`, `--target-userid`
- file: `--source-file`, `--target-file` - which take the corresponding certificate from a file instead of the certificate store.

`sq pki vouch replay` checks if the target certificate shares at least one User ID with the source certificate and aborts if it fails. `--allow-dissimilar-userids` skips this check.

The replay of certifications is limited to active certifications. Certifications made for (now) expired, revoked or invalid certificates are skipped.
