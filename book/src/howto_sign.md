## How to sign a file or message

To sign a file with an inlined signature, use the following command:

```shell
$ sq sign --signer $FINGERPRINT $FILE
```



To separate the signature from the file, make a detached signature as follows:   


```shell
$ sq sign --signer $FINGERPRINT --signature-file $FILE
```


> More information is available here: [Chapter 'Sign files'](./signing.md)
