## How to retrieve a certificate

To retrieve a certificate from a keyserver, use the `sq network search $PATTERN` subcommand for example as follows:


```shell
$ sq network search alice@example.org
```

> To get more information like customizing the list of keyservers etc., see chapter ['Search a certificate in the internet'](./cert_retrieve.md)

