## Create a certificate

> Intended Content
> - creation happened when a key got created, what is needed is an export from the key 
> - export a certificate from a key

> Suggestion: move that to Authenticity


#### Import a certificate to GNUPG

To import a cert to the GnuPG Keyring, you have to export it first from the cert store:


```shell
$ sq cert export alice@example.com | gpg --import
```
