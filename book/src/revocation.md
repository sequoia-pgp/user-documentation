## Retracting a key

A key has a certain lifespan, outside of which it is no longer valid. This either can be the case when you don't want to extend the expired key because you no longer want to use it, or if you want to terminate the expiration time prematurely. In a more severe case, you have to end it because you lost the key or it has been compromised.  

Since you not only use the key yourself, but in most cases also have given the certificate - the public part of the key - to other people or uploaded it to keyservers, you would have to revoke it. 

This is because it's important to let the world know about the invalidity, to prevent future usage such as encrypting data with that certificate. Otherwise people would still use the certificate to encrypt messages, which then could not be decrypted, or, in case of compromise, someone else might be able to read it. 

> Note: The expiration of the certificate does not affect the secret key material, encrypted data can still be decrypted.


### Hard and soft revocation

Revocation is done by creating a revocation certificate and publishing this or your revoked certificate to the keyservers. A revocation certificate is stand-alone, so you can, but don't have to add it to a key to publish it. The keyservers would add it to the origin certificate and people will get this revoked certificate from there. Nevertheless, `sq key revoke` will automatically mark your own key as revoked while creating the revocation certificate, so you don't have to apply it and can just disseminate your updated certificate. If your certificate is not published on keyservers, but only passed by lets say email, you could either simply send the revocation certificate itself to somebody to import it (importing will automatically add it to the certificate) or you could send the already revoked certificate. 

There are two different ways a revocation can be looked at. They concern the validity of signatures and certifications created by the revoked key. If the key got compromised, meaning an attacker got control over the secret key material, new signatures/certifications could be created by the attacker (and probably even dated back in time). This implies that signatures and certificates made by a compromised key cannot be trusted (even if they have been made before the compromise).

But keys do not have to be compromised to be revoked, keys can also be retired or superseded - the secret key material in this case would still be under the sole control of the keyholder. Signatures and certifications keep their validity.

So there are two classes of revocations. The revocation in case of a compromise that invalidates signatures and certifications is call "hard". The other - the retirement case - is called "soft".


The revocation certificate which is generated at creation time of the key is an emergency measure in case you lost your key. That's unrelated to any possible breach. This certificate has been stored in the $HOME/.local/share/sequoia/revocation-certificates (unless stated otherwise as described below) and it is tagged as 'unspecified' in the revocation certificate, which cannot be changed later on. If you still have access to your key (even compromised), you can (and should) generate a more meaningful revocation certificate. Besides the revocation reason this also includes - if intended - an announcement of a new certificate's fingerprint, using the 'message' option.

In both cases you have the possibility to write the revocation certificate to a file. In the case of the emergency revocation certificate the option '--rev-cert' followed by a file name has to be provided while generating your key. It then can be copied to another data store so that you can still access it, even if your key on your computer is not available anymore. When creating a soft revocation certificate, for example in order to have a more specified one prepared for future usage, you would have to use the '--output' option to write that revocation certificate to a file instead of importing it directly into the keystore, which would happen, if you don't use this option. In that case your key would automatically be revoked by sq. If you're storing it as a file, you would have to import it into the keystore to add it to the key, or publish it by itself as described below.

###  Creating and publishing a revocation certificate for soft revocation

As described above, you can generate a revocation certificate for a certificate using the command `sq key revoke`.

In the following example `--cert` requires the fingerprint of the cert to be revoked. The last two options `<REASON>`, where to choose from `compromised`, `superseded`, `retired` or `unspecified`, and `<MESSAGE>`  are mandatory.

```shell
$ sq key revoke --cert $FINGERPRINT --reason retired --message 'quit my job'
```

This command does not give any output. If you have a look into the certificate using the origin fingerprint, you will see that is is marked as revoked. The revocation certificate has been created and at the same time imported into the keystore and applied on the key, so that the inspection shows the certificate which is marked as revoked:


```shell
sq inspect --cert $FINGERPRINT
OpenPGP Certificate.

    Fingerprint: $FINGERPRINT
                 Revoked:
                  - Retired
                    On: 2024-12-05 13:57:43 UTC
                    Message: "quit my job"

[...]
```



To create a revocation certificate for later usage, use the `--output` switch as follows:


```shell
$ sq key revoke --cert $FINGERPRINT --reason $REASON --message $MESSAGE --output $FILE
```

This stores the revocation certificate in a file - **but does not change the key**, it's still not revoked!
To use this revocation cert, import it: 

```shell
$ sq cert import $FILE
```

then the key is revoked.



### Publishing


After creating the revocation certificate, you should publish the revoked certificate on the keyservers:

```shell
$ sq network keyserver publish --cert $FINGERPRINT
```

As described in the beginning of this chapter, you can also publish the revocation certificate itself if you saved it in a file:

```shell
$ sq network keyserver publish FILE 
```

> Publishing is dealt in this [chapter](./cert_publish.md) 

###  Revoke a User ID

It's also possible to revoke a User ID such as a name or an email address using the subcommand `userid`. The process is very similar to the process of generating and publishing a revocation certificate: 

```shell
$ sq key userid revoke --cert $FINGERPRINT --userid 'alice' --reason retired --message 'changed my name'
$ sq inspect --cert $FINGERPRINT
OpenPGP Certificate.

[...]

         UserID: alice
                 Revoked:
                  - User ID information is no longer valid
                    On: 2024-10-03 06:20:09 UTC
                    Message: "changed my name"
[...]
$ sq network keyserver publish --cert $FINGERPRINT 
```



## Delete a key

There are more specific use cases in which someone wants to keep the certificate valid, but the secret key material should be deleted. 

If you for example work in stateless mode and manage keys only in files, the way to extract the certificate from a key would be to delete the secret key material from that file( in case you want to use both of them afterwards, it's necessary to make a copy of the key beforehand). 
A second use case could arise during the process of a (sub)key rotation. It would improve confidentiality to delete the secret key material for the sake of not being able to become infiltrated at some point in the future, which would otherwise effect previous sent and stored messages. 

For this cases, `sq` offers the command `sq key delete` to delete the secret key material from a key, what remains is the certificate:


```shell
$ sq key delete --cert $FINGERPRINT
```

Also possible is to provide the User ID that includes a specific email address, which should be done carefully if there exist more than one key containing this User ID:

```shell
$ sq key delete --cert-email alice@example.org
```

