## Upgrading a certification signature from SHA1 to SHA256

A certification signature is a way to store the assertion that a binding between a User ID (like an email address) and a public key is authentic. The certification signature is added to the certificate containing the binding. There are basically two types of certifications: self-signed ones which signify, that the certification aligns with the intends of the keyholder and certifications made by others, signifying their confidence in the authenticity of the binding.

The first certification signature is added to a key when it's generated. It selfsigns the key creating a binding between its public key and its initial User IDs. Revocation certificates also contain a certification signature for verifying the authenticity of the revocation.

The certification signature is created by hashing the public key and the User ID, and subsequently signing the resulting hash value. Ideally the hash value for the combination of public key and User ID should be unique. Hash values however are of limited size, which implies an information loss – there exist other input values, which produce the same hash value. The consequence of these collisions is that a certification signature asserts authenticity of an unlimited amount of public key - User ID pairs. Obviously this is not intended.

One design criteria for a hash function used for the creation of certification signatures is that these collisions are next to impossible to construct making the abuse of a collision infeasible in practice.

SHA1 is one of such hash functions. Over the time careful research concluded that SHA1 was less robust against collision construction than initially thought. Combined with the expanding availability of computing power SHA1 became vulnerable, meaning the construction of a fitting public key – User ID pair for a hash value became a realistic scenario. The remedy is to replace a SHA1 hash with a hash calculated by a stronger hash function: SHA256. 

### Check for SHA1 signatures in certificates

We use `sq cert lint` to check for certificates that use SHA1 signatures. `sq cert lint` takes the certificate(s) to examine either a file parameter or from `stdin`.

> This example takes the certificates from gpg, using a keyring which contains just one vulnerable certificate.

```shell
$ gpg --export | sq cert lint
Certificate B371C0977CFE0EBC is not valid under the standard policy: No binding signature at time 2025-02-17T14:52:16Z
Certificate B371C0977CFE0EBC contains a User ID (Alice Example <alice@example.com>) protected by SHA-1
Certificate B371C0977CFE0EBC, key 395B0E508A299D30 uses a SHA-1-protected binding signature.
Examined 1 certificate.
  0 certificates are invalid and were not linted. (GOOD)
  1 certificate was linted.
  1 of the 1 certificates (100%) has at least one issue. (BAD)
0 of the linted certificates were revoked.
  0 of the 0 certificates has revocation certificates that are weaker than the certificate and should be recreated. (GOOD)
0 of the linted certificates were expired.
1 of the non-revoked linted certificate has at least one non-revoked User ID:
  1 has at least one User ID protected by SHA-1. (BAD)
  1 has all User IDs protected by SHA-1. (BAD)
1 of the non-revoked linted certificates has at least one non-revoked, live subkey:
  1 has at least one non-revoked, live subkey with a binding signature that uses SHA-1. (BAD)
0 of the non-revoked linted certificates have at least one non-revoked, live, signing-capable subkey:
  0 certificates have at least one non-revoked, live, signing-capable subkey with a strong binding signature, but a backsig that uses SHA-1. (GOOD)

  Error: 1 certificate have at least one issue
```

To fix a certification signature a secret key is needed. 

> Exporting a "secret key" actually exports the keypair - the secret key material and the public certificate.

Passing `--fix` to `sq cert lint` causes `sq` to generate new certifications using SHA256 as hash function. The result is written to `stdout` and can be passed to `sq` or `gpg` for import. 

```shell
$ gpg --export-secret-keys alice@example.com | sq cert lint --fix
Certificate B371C0977CFE0EBC is not valid under the standard policy: No binding signature at time 2025-02-17T14:56:10Z
Certificate B371C0977CFE0EBC contains a User ID (Alice Example <alice@example.com>) protected by SHA-1
Please enter the password to decrypt B371C0977CFE0EBC/B371C0977CFE0EBC Alice Example <alice@example.com> (UNAUTHENTICATED): 
Certificate B371C0977CFE0EBC, key 395B0E508A299D30 uses a SHA-1-protected binding signature.
-----BEGIN PGP PRIVATE KEY BLOCK-----

xcMGBGezSJYBCACv2FM2TzZtv5LmV+CmKjRqrcuaexasLg6GxpoTHVN7gwSXDNGq
[...]
2DIgwY1O
=hUqr
-----END PGP PRIVATE KEY BLOCK-----
Examined 1 certificate.
  0 certificates are invalid and were not linted. (GOOD)
  1 certificate was linted.
  1 of the 1 certificates (100%) has at least one issue. (BAD)
0 of the linted certificates were revoked.
  0 of the 0 certificates has revocation certificates that are weaker than the certificate and should be recreated. (GOOD)
0 of the linted certificates were expired.
1 of the non-revoked linted certificate has at least one non-revoked User ID:
  1 has at least one User ID protected by SHA-1. (BAD)
  1 has all User IDs protected by SHA-1. (BAD)
1 of the non-revoked linted certificates has at least one non-revoked, live subkey:
  1 has at least one non-revoked, live subkey with a binding signature that uses SHA-1. (BAD)
0 of the non-revoked linted certificates have at least one non-revoked, live, signing-capable subkey:
  0 certificates have at least one non-revoked, live, signing-capable subkey with a strong binding signature, but a backsig that uses SHA-1. (GOOD)
```

The output of `sq cert lint --fix` can be piped to `sq key import` or `gpg --import`. If using `gpg` like

```shell
$ gpg --export-secret-keys alice@example.com | sq cert lint --fix | gpg --import
[...]
gpg: Total number processed: 1
gpg:         new signatures: 2
gpg:       secret keys read: 1
gpg:  secret keys unchanged: 1
```

Two new signatures are detected, these are the ones with the improved hash algorithm. 

We now have a look at the cert again, expecting it to be linted:

```shell
$ gpg --export alice@example.com | sq cert lint
Examined 1 certificate.
  0 certificates are invalid and were not linted. (GOOD)
  1 certificate was linted.
  0 of the 1 certificates (0%) have at least one issue. (GOOD)
0 of the linted certificates were revoked.
  0 of the 0 certificates has revocation certificates that are weaker than the certificate and should be recreated. (GOOD)
0 of the linted certificates were expired.
1 of the non-revoked linted certificate has at least one non-revoked User ID:
  0 have at least one User ID protected by SHA-1. (GOOD)
  0 have all User IDs protected by SHA-1. (GOOD)
1 of the non-revoked linted certificates has at least one non-revoked, live subkey:
  0 have at least one non-revoked, live subkey with a binding signature that uses SHA-1. (GOOD)
0 of the non-revoked linted certificates have at least one non-revoked, live, signing-capable subkey:
  0 certificates have at least one non-revoked, live, signing-capable subkey with a strong binding signature, but a backsig that uses SHA-1. (GOOD)
```
All checks are good.

For a closer look, `sq packet dump` can be used.

```shell
$ gpg --export alice@example.com | sq packet dump
Public-Key Packet, old CTB, 269 bytes
    Version: 4
    Creation time: 2025-02-17 14:32:54 UTC
    Pk algo: RSA
    Pk size: 2048 bits
    Fingerprint: 17D714014EE9789B1152A408B371C0977CFE0EBC
    KeyID: B371C0977CFE0EBC
  
User ID Packet, old CTB, 33 bytes
    Value: Alice Example <alice@example.com>
  
Signature Packet, old CTB, 312 bytes
    Version: 4
    Type: PositiveCertification
    Pk algo: RSA
    Hash algo: SHA1
    Hashed area:
      Signature creation time: 2025-02-17 14:32:54 UTC
      Key flags: CS
      Symmetric algo preferences: AES256, AES192, AES128, CAST5, TripleDES
      Hash preferences: SHA256, SHA1, SHA384, SHA512, SHA224
      Compression preferences: Zlib, BZip2, Zip
      Features: SEIPDv1
      Keyserver preferences: no modify
    Unhashed area:
      Issuer: B371C0977CFE0EBC
        Alice Example <alice@example.com> (UNAUTHENTICATED)
    Digest prefix: A68C
    Level: 0 (signature over data)
  
Signature Packet, old CTB, 402 bytes
    Version: 4
    Type: PositiveCertification
    Pk algo: RSA
    Hash algo: SHA256
    Hashed area:
      Signature creation time: 2025-02-17 14:59:11 UTC (critical)
      Symmetric algo preferences: AES256, AES192, AES128
      Issuer: B371C0977CFE0EBC
        Alice Example <alice@example.com> (UNAUTHENTICATED)
      Notation: salt@notations.sequoia-pgp.org
        00000000  77 d1 f6 d5 be 0b 50 60  c2 02 e9 db 20 0e 38 37
        00000010  04 7a 3d 06 58 fc 7c 16  de 66 86 d2 e1 b5 29 37
      Hash preferences: SHA256, SHA512
      Compression preferences: Zlib, BZip2, Zip
      Keyserver preferences: no modify
      Key flags: CS
      Features: SEIPDv1
      Issuer Fingerprint: 17D714014EE9789B1152A408B371C0977CFE0EBC
        Alice Example <alice@example.com> (UNAUTHENTICATED)
    Digest prefix: 7F53
    Level: 0 (signature over data)
  
Public-Subkey Packet, old CTB, 269 bytes
    Version: 4
    Creation time: 2025-02-17 14:32:54 UTC
    Pk algo: RSA
    Pk size: 2048 bits
    Fingerprint: 8CE181686CF93037B5971F22395B0E508A299D30
    KeyID: 395B0E508A299D30
  
Signature Packet, old CTB, 287 bytes
    Version: 4
    Type: SubkeyBinding
    Pk algo: RSA
    Hash algo: SHA1
    Hashed area:
      Signature creation time: 2025-02-17 14:32:54 UTC
      Key flags: EtEr
    Unhashed area:
      Issuer: B371C0977CFE0EBC
        Alice Example <alice@example.com> (UNAUTHENTICATED)
    Digest prefix: 40E8
    Level: 0 (signature over data)
  
Signature Packet, old CTB, 382 bytes
    Version: 4
    Type: SubkeyBinding
    Pk algo: RSA
    Hash algo: SHA256
    Hashed area:
      Signature creation time: 2025-02-17 14:59:11 UTC (critical)
      Issuer: B371C0977CFE0EBC
        Alice Example <alice@example.com> (UNAUTHENTICATED)
      Notation: salt@notations.sequoia-pgp.org
        00000000  1d f1 d2 67 0e 98 6a 8c  f9 ab 62 c0 44 fc cf 17
        00000010  41 5e 64 9d 63 5a 75 d2  13 82 73 3b 08 7d 5c ec
      Key flags: EtEr
      Issuer Fingerprint: 17D714014EE9789B1152A408B371C0977CFE0EBC
        Alice Example <alice@example.com> (UNAUTHENTICATED)
    Digest prefix: 94F9
    Level: 0 (signature over data)
```

Note that the old SHA1 certifications are still within the certificate.

### Fixing a revocation certificate

First, lets look into a revocation certificate to examine the hash algorithm used. In this example `alice.rev` contains the revocation certificate. Use `sq packet dump` to have a closer look.

```shell
$ sq packet dump alice.rev
Signature Packet, old CTB, 293 bytes
    Version: 4
    Type: KeyRevocation
    Pk algo: RSA
    Hash algo: SHA1
    Hashed area:
      Signature creation time: 2025-02-17 15:10:37 UTC
      Reason for revocation: Key is retired and no longer used, Retire
    Unhashed area:
      Issuer: B371C0977CFE0EBC
        Alice Example <alice@example.com> (UNAUTHENTICATED)
    Digest prefix: E7F3
    Level: 0 (signature over data)
```
`Hash algo` tells the used algorithm, SHA1 in this case.

`sq cert lint` is not meant to fix this issue. Instead simply create a new revocation certificate.

> Note: The generation of a revocation certificate only works with keys containing SHA256 certifications. See above how fix keys in case they are missing.

```shell
$ gpg --export-secret-key alice@example.com | sq key userid revoke --cert-file - --userid "Alice Example <alice@example.com>" --reason "retired" --message "This key is retired" --output alice_new.rev
Waiting for OpenPGP certificates on stdin...
Please enter the password to decrypt B371C0977CFE0EBC/B371C0977CFE0EBC Alice Example <alice@example.com> (UNAUTHENTICATED): 
$ sq packet dump alice_new.rev
Public-Key Packet, new CTB, 269 bytes
    Version: 4
    Creation time: 2025-02-17 14:32:54 UTC
    Pk algo: RSA
    Pk size: 2048 bits
    Fingerprint: 17D714014EE9789B1152A408B371C0977CFE0EBC
    KeyID: B371C0977CFE0EBC

User ID Packet, new CTB, 33 bytes
    Value: Alice Example <alice@example.com>

Signature Packet, new CTB, 401 bytes
    Version: 4
    Type: CertificationRevocation
    Pk algo: RSA
    Hash algo: SHA512
    Hashed area:
      Signature creation time: 2025-02-17 15:15:34 UTC (critical)
      Issuer: B371C0977CFE0EBC
        Alice Example <alice@example.com> (UNAUTHENTICATED)
      Notation: salt@notations.sequoia-pgp.org
        00000000  3f 86 99 1e cb 7a 33 0d  39 f0 da 7c 99 00 be 22
        00000010  42 04 f9 d5 83 a9 b0 36  33 1c 88 07 34 2d 0d 18
      Reason for revocation: User ID information is no longer valid, This key is retired
      Issuer Fingerprint: 17D714014EE9789B1152A408B371C0977CFE0EBC
        Alice Example <alice@example.com> (UNAUTHENTICATED)
    Digest prefix: D216
    Level: 0 (signature over data)
```
