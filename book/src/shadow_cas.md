## Shadow CAs

> - Internal mechanism to record the provenience of a cert
> - hierarchically stacked
> - the internal trust root and `sq pki link add`


> - trust root
> - public directory
> - keyserver
> - WKD
> - DANE

### Purpose

- establishing authentication
  - a certificate/user id binding is considered authentic if a path exists starting from the trust root to that binding, that is a precondition
  - the degree of authenticity depends on the calculated amount of "trust". The calculation is done over the paths (there might be more that just one).
  - if there is no path, binding is considered not authentic - not matter what
  - a user can create a path him-/herself with `sq pki link add...`

- record origin
  - a series of shadow CAs are created on demand to record the origin of a certificate
  - these CAs can be adjusted like other CAs, they are equipped with a trust amount, can be scoped to domains, can be limited in their trust depth

### Usage

- `sq pki list` 

- `sq pki list alice@example.com`

- `sq pki link add --ca ... $FPR_OF_SHADOW_CA`

- `sq network search $URL_OF_CERTIFICATE`
   - `sq pki list $FPR_OF_CERT` - shows new shadow CA
   - shadow CAs have a path to the trust root
