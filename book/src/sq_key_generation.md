
## Key generation


### Generate a key 

You can generate a key with different parameters and for different purposes. The default settings include an expiration of 3 years and elliptic curves as cryptographic algorithm. All of them can be modified.

> Just a quick note at this point: it is not possible to explicitly generate a certificate. You only ever generate a key, as a certificate is always a part of a key, containing only public key material. 

Using the `sq key generate` command is a stateful operation, which imports the key directly into the key store. If you want to avoid this, you could also use the `--output` option to specify a location for the key file. The co-generated emergency revocation file will automatically be stored in `$HOME/.local/share/sequoia/revocation-certificates` (see chapter ['File structure and backup'](/files.md) for more information ) - as long as you don't state otherwise.

You don't have to separate different User IDs, but could instead combine them to one statement `--userid 'alice <alice@example.com>'`. However, this is not the recommended practice, instead Sequoia suggests to have separate User IDs for name and email address, which allows them to be certified or being revoked separately.

```shell
$ sq key generate --own-key --name alice --email alice@example.com 
```

This example generates a key with the two User IDs `alice` and `<alice@example.com>`. `--own-key` tells `sq` to promote the new key as an unconstrained trust introducer. If you plan to share the key (for instance to use it as a group key), use `--shared-key` instead - this way the new key will not be made a trust introducer.

You can use `sq pki link {add | authorize | retract}` to change the trust introducer state at a later time. 

For understanding in detail what `sq pki link` does, see chapter ['Authenticating certificates'](auth_certs.md).



##### Choosing a cipher-suite

The default cipher suite that sq uses when generating a key is `cv25519`. To select a specific suite, use `--cipher-suite` with one of the following options:
> - **rsa2k** RSA with 2048 bits 
> - **rsa3k** RSA with 3072 bits 
> - **rsa4k** RSA with 4096 bits 
> - **cv25519** Elliptic curve of the same name with 256 bits

To show it with an example:

```shell
$ sq key generate --own-key --name alice --email alice@example.com  --cipher-suite rsa4k
```

##### Opting out password protection

The default settings for generating a key include password protection for the key material, which is queried after entering the `sq key generate` command. In order to create a key without password protection, you have to specify the `--without-password` option.

```shell
$ sq key generate --own-key --name alice --email alice@example.com --without-password 
```


##### Choose expiration time

The default setting is an expiration time of 3 years. To define a different one, you can either choose any ISO 8601 string or a custom duration using `N[ymwds]` (both with a possible reference time using `--time`). You can also change the expiration date at a later point to prolong or shorten the period of time. See chapter ['Maintaining a key'](key_maintain.md) for more information. For more thoughts on why setting an expiration date is a useful thing, see ['Key expiration'](key_expiration.md).

```shell
$ sq key generate --own-key --name alice --email alice@example.com --expiration 2y
```

> Note: All of the key generating options showed above (and more) can be combined as follows:

```shell
$ sq key generate --own-key --name alice --email alice@example.com --expiration 2y --cipher-suite rsa4k
```

##### Generating v6 keys

Version 6 (v6) keys are introduced with RFC9580, which is implemented in version 2.0.0 of Sequoia PGP and used by `sq` since version 1.3.0. To generate a v6 key use the `--profile` option:

```shell
$ sq key generate --profile rfc9580 ...
```

Available values are `rfc4880` and `rfc9580`. `rfc4880` is the default value, which causes `sq` to generate a v4 key.
