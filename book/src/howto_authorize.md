## How to mark a certificate as a trusted introducer


To mark a certificate as an exportable trust introducer (respectively a certificate authority) for the domain `example.com` with a preconfigured trust depth of 1, use `sq pki vouch authorize` as follows:

```shell
$ sq pki vouch authorize --certifier $FPR_CERTIFIER --cert $FPR_TO_BE_CERTIFIED --domain=example.com
```

> More information can be found here: [Chapter 'Certify and authorize certificates'](./cert_certify.md)



