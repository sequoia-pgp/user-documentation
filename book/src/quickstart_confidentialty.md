## Confidentiality - Quick start 


Here you find a quick walk through on the topics of key generation and typical usages of certificates.


### Generating a key 

To generate a key with sq, you can use the following command: 

```shell
$ sq key generate --own-key --name 'alice' --email 'alice@example.com' 
```



### Export a key from the keystore into a file


```shell
$ sq key export --cert $FINGERPRINT > $KEYFILE
```

### Import a key into keystore

If a key is only available as a file, it can be imported into the keystore:

```shell
$ sq key import $KEYFILE
```



### Encrypt and decrypt  a file

To encrypt a message or a file with another person's certificate, you can use 'sq encrypt' as it is shown here: 


```shell
$ sq encrypt --for-email alice@example.com message.txt
```

To decrypt a file using a key in the keystore, you can use 'sq decrypt' as follows:


```shell
$ sq decrypt message.pgp
```





