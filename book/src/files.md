## Files

### Directories

Sequoia PGP (and therefore `sq`) stores its state - mainly keys and certificates - in a directory structure. The default locations are:

- the certificate store (short cert store): `$HOME/.local/share/pgp.cert.d`
- the key store: `$HOME/.local/share/sequoia/keystore`
- the revocation certificate store: `$HOME/.local/share/sequoia/revocation-certificates`

These default locations can be changed by setting the environment variable `SEQUOIA_HOME`. There are subtle changes in the directory structure when using `SEQUOIA_HOME`:

- the certificate store is: `$SEQUOIA_HOME/data/pgp.cert.d`
- the key store is: `$SEQUOIA_HOME/data/keystore`
- the revocation certificate store is: `$SEQUOIA_HOME/data/revocation-certificates`

If `$SEQUOIA_HOME` equals `$HOME`, then the default directory structure applies, as if `SEQUOIA_HOME` is not set. If `SEQUOIA_HOME` is set to `none`, `sq` will not use any key or cert store. 

The location of the cert store can be overridden by setting `PGP_CERT_D` or `SEQUOIA_CERT_STORE`. `SEQUOIA_CERT_STORE` has precedence over `PGP_CERT_D`. Both override the implied setting from `SEQUOIA_HOME` - even if it was set to `none`.

The location of the key store can be overridden by setting `SEQUOIA_KEY_STORE`, there is no second environment variable in this case. This will also override `SEQUOIA_HOME`.

All these locations can also be specified on the command line, which will override the environment variables.

#### Keystore

The keystore contains keypairs, public and secret key material, together with User IDs and further metadata bundled together in files. These files are located in the subdirectory `softkeys` - 'hard keys' are keys stored on specialized hardware like smart cards.

#### Certificate store

The certificate store contains all certificates imported via `sq cert import` or fetched by `sq network search`. It also contains the keys for the shadow CAs, the local trust root and a SQLite database for cert lookup. 

#### Revocation certificate store

Revocation certificates are created when keys are created. When a key is generated in the keystore, its revocation certificate gets stored in the revocation certificate store. 

> This revocation certificate is unspecific on the reason for revocation (which equals to 'compromised'). It's meant as a last resort, if the original key is lost and a more specific revocation cannot be created. 

### Backup

`sq` (and Sequoia PGP) do not have in-memory stores or caches, which means that creating a backup of the files in the stores is sufficient.

