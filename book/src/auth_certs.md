## Authenticating certificates

In case you ever want to send an encrypted email to someone, you will need public key material to do the encryption. Public key material is part of a certificate, getting the right material is equivalent to getting the right certificate. Certificates also contain User IDs, claiming that the corresponding user published this certificate and that the public key material within is indeed the right one to use. This suggests that once you know the ID of a user (for instance an email address), identifying the right certificate should be easy.

[User IDs](./user_id.md) however are just claims. Claims which need to be verified before it's advisable to use any public key material. This process of verification is called **authentication**. This process however is blurry - how much "evidence" do I need to rely on the claimed binding between a User ID and the public key material?

In case the intended receiver of my email created and published a certificate, there exists a "right" certificate. Certificates have unique, unambiguous identifiers called fingerprints. In contrast to User IDs, these are not claims but checksums over (some of) the content. Knowing the fingerprint allows to identify a certificate with certainty. 

This shifts the problem from identifying the right certificate to identifying the right fingerprint, fingerprints however are much smaller - small enough to for instance print on a business card. The downside of fingerprints is that they are completely unintuitive sequences of characters with high entropy and no redundancy - they have no resemblance with a User ID.

### Identifying a certificate - the waterproof way

Assuming that you are certain to have the right fingerprint - lets name it `$FPR`. Retrieving the corresponding certificate can be done with:

```shell
$ sq network search $FPR
```

This downloads the certificate from sources in the internet and stores it in the local certificate store. More details on [retrieving can be found here](./cert_retrieve.md).

The next step is to tell `Sequoia PGP` that the binding between a User ID and this certificate is valid. This might come as a surprise as we skip the manual comparison of fingerprints - let `sq` do the job:

```shell
$ sq pki link add --cert $FPR --userid $USER_ID
```

Since `sq network search $FPR` retrieved a certificate with that fingerprint, all what is left to do is to verify if this certificate contains the wanted User ID. `sq pki link add` does that and will return an error, if that User ID is not present (or you made a typo in the fingerprint). 

After this step everything is set up to proceed with the encryption of the email.

By the way - you can get the fingerprint of your own key by

```shell
$ sq key list $USER_ID
```

### Identifying a certificate - softer approaches

The scenario in the preceding chapter is a bit idealistic - it assumes that
- you are in possession of the right fingerprint 
- the corresponding certificate has been published on a well known server in the internet

Usually circumstances are less optimal. But, however you got the fingerprint (or certificate), this happened in a context which can be taken into account - used as evidence. 

If you got a fingerprint printed on a slip of paper handed over by the alleged creator of the corresponding key, there is high evidence that you got the right fingerprint. Same could be said if you got the certificate itself on a usb stick.

Things become less reliable if the fingerprint reached you in an email, especially if that email is not signed. If that email came as response to a request ("Please send me your certificate ..."), it adds to its trustworthiness. If you even know the sender personally, this might be another plus. 

Some keyservers require an attestation. They send an email to each User ID in a certificate which looks like an email address, containing a link. Clicking that link leads to sending a specific request back to the server, which is taken as a confirmation of the authenticity of the User ID - certificate binding. If you get a certificate from such a server, you can take this into account.

Some websites publish alongside their contact email address the corresponding fingerprint or even the certificate. Assuming that only a limited number of people are able to modify the content of this website and all of them have honest intentions, this can be taken as evidence of authenticity.

However, all the above examples (and there are many more) are circumstantial. They reflect social relationships, assumptions on integrity and diligence and probably more.

On top of this: Not every email is send in a life or death (or prison) situation. Many of them are quiet mundane, for some it might be even acceptable to send them unencrypted.

All of this makes trade offs a viable option. It affects the amount of evidence one wants to see before relying on a certificate. These trade offs are probably the main way how "authentication" is handled. 

### Using the result

Once you are willing to rely on a certificate, you have to add a link in the PKI of Sequoia PGP. This enables Sequoia PGP to remember your decision and treat the certificate as authenticated from that moment onward.

Adding a link is done by

```shell
$ sq pki link add --cert $FPR --userid $USER_ID
```
Alternatively to `--userid`, also `--email` would work. If instead you specify `--all`, all bindings in the designated certificate will be added.


This command does several things:

It's adding a signature to the certificate. This signature indicates that you consider the binding between the certificate and the User ID authentic. Future invocations of `sq` (or any other software using Sequoia PGP) 
will recognize this signature and the implied authenticity. You can see the effect by:

```shell
$ sq pki link list --cert-email alice@example.com
 - ┌ 60B00B3173854BA69689B19CCAC5C827BE11485F
   └ "<alice@example.com>"
     - is linked

 - ┌ 60B00B3173854BA69689B19CCAC5C827BE11485F
   └ "Alice"
     - is linked

```

The key used to generate the signature is not an arbitrary key, but the **trust root** key located in the certificate store. For details see chapter [Shadow CAs](./bckgrnd_shadow_cas.md). 

The above command will also add a **trust-depth** to the certificate - User ID binding. Using `sq pki link add` like above the trust depth will be 0, which means that the implied authenticity by the signature does not "spread out" - it's limited to exactly this binding.  

A trust depth of 1 means that not only the specific binding is considered authentic, but also all certifications made by this certificate.

Consider the following example:
<blockquote>

There are 4 certificates (A, B, C, D), each with just one User ID - so there are these bindings:
- Alice's certificate`[ A, alice@example.com ]`
- Bob's certificate `[ B, bob@example.com ]`
- Carol's certificate `[ C, carol@example.com ]`
- Dave's certificate `[ D, dave@example.com ]`

Assuming that Alice is certain that Bob's certificate (especially the binding `[ B, bob@example.com ]`) is correct and Bob is equally certain about Carol's certificate, as is Carol of Dave's - each of them certifies the corresponding certificates:
- Alice's certificate`[ A, alice@example.com ]`
- Bob's certificate `[ B, bob@example.com ](certified using A)`
- Carol's certificate `[ C, carol@example.com ](certified using B)`
- Dave's certificate `[ D, dave@example.com ](certified using C)`

If Alice certifies Bob's certificate with a trust depth of **zero**, then - from Alice's perspective - Bob's certificate is authenticated. That's it.

If Alice would use a trust depth of **one**, then not only Bob's certificate is considered authenticated, but also Carol's, because Carol's certificate has a certification made with Bob's certificate. By assigning a trust depth of one, Alice marks Bob as a **trust introducer** - his certifications are now as good as if Alice would have done the certifications herself.

Carol certified Dave's certificate, from the perspective of Alice however, Dave's certificate is **not** authenticated. A trust depth of one only effects the immediate certifications from Bob. To also include Dave's certificate, Alice has to assign Bob a trust depth of **two**, thus allowing Bob to also introduce trust introducers to Alice - in this case: Carol. Her certification of Dave's certificate would then be taken into account by Alice. 

To visualize the effect of trust depth:
<table style="margin: 4px; border-spacing: 1em 0">
<tr><th>trust-depth</th><th>effected certificates</th></tr>
<tr><td>0</td><td>Bob</td></tr>
<tr><td>1</td><td>Bob, Carol</td></tr>
<tr><td>2</td><td>Bob, Carol, Dave</td></tr>
</table>

Please note that Alice is assigning the trust depth when certifying Bob's certificate - she doesn't have to touch Carol's certificate (or even Dave's) - to achieve the effect. Actually they don't have to be in Alice's local certificate store, they don't even have to exist at the time of the certification. They have to be locally available, when Alice calculates the authenticity of the certificates. 

The trust depth is a counter which indicates how many hops in a certification chain will be covered by the initial assertion of authenticity. The maximum depth is 255.

</blockquote>

The above `sq pki link add` with a trust depth of 1 looks like (note the `authorize`):

```shell
$ sq pki link authorize --depth 1 --cert $FPR --email bob@example.com
...
```

Certificates can have more than one User ID. Instead of treating each User ID separately - repeating `sq pki link add` for each of them - you can pass `--all` at the command line. 

```shell
$ sq pki link add --cert $FPR --all
```

Links created by `sq pki link add` will not expire, this can be changed by using `--expiration DATE` - `DATE` can either by in ISO 8601 format (2025-01-01) or a time interval like `3y` (3 years).

```shell
$ sq pki link add --cert $FPR --all --expiration 3y
```

Links can be retracted at any point in time. Use

```shell
$ sq pki link retract --cert $FPR --email bob@example.com
```

to retract a specific certificate - User ID binding, or

```shell
$ sq pki link retract --cert $FPR --all
```

as a convenience to remove all links associated with the given certificate.


The existing links within the PKI can be listed by:

```shell
$ sq pki link list
 - ┌ 042C3AA73E1566E90FDC31B05C1EA3A3B894010E
   └ "Public Directories"
     - is linked as a partially trusted CA
     - trust amount: 40

 - ┌ 4D735E0D7DDE1B338E05F2B80AF4CE6DF697FB28
   └ "<alice@example.com>"
     - is linked

 - ┌ 4D735E0D7DDE1B338E05F2B80AF4CE6DF697FB28
   └ "Alice"
     - is linked

 - ┌ D4A2E8E858B215BD4E0A775C2904DCFB85ED0E9B
   └ "<bob@example.com>"
     - is linked

 - ┌ D4A2E8E858B215BD4E0A775C2904DCFB85ED0E9B
   └ "Bob"
     - is linked
```

This example shows a list with two certificates, each of them has two User IDs. Additionally the shadow CA for "Public Directories" is displayed. 
