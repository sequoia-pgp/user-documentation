## About sq and Sequoia PGP

Sequoia PGP is an implementation of the OpenPGP standard written in Rust. It comes in the form of a library that other applications can integrate to use OpenPGP functionality without reinventing the wheel. `sq` is a command line application using Sequoia PGP in such a way. 

`sq` aims to expose a useful set of OpenPGP functionality for common tasks: encryption and decryption, signing and verifying, key and certificate management, certification and the like.  

OpenPGP is a complex and elaborated standard, addressing issues of privacy and authenticity. As such it's not easy to digest, `sq` delivers an opinionated approach, making OpenPGP accessible and (hopefully) easy to use without sacrificing the goals of OpenPGP.

`sq` aims to **safe by default**, that doesn't mean that you cannot do stupid or dangerous things with `sq`, but the easy, default way of using `sq` is the safe way.

Sequoia PGP implements RFC9580 (published in 2024, also known as v6) since version 2.0.0, which is used by `sq` since version 1.3.0. 

### Design and specialties 

`sq` (and Sequoia PGP for that matter) uses a specific terminology when addressing keys (public and private) - please make sure to read the chapter ['Getting started'](./getting_started.md) for an overview.

There is no "automatic trust". Having a (secret) key in your key store doesn't imply that you "trust" signatures and certifications issued by that key. There is an extra step necessary, which protects against unwanted consequences of inadvertently imported keys. The same holds true for certificates ('public keys'), `sq` wants you to be certain when designating a certificate.  

`sq` doesn't use keyrings (by default), there are stores. While the key store operates similar to a keyring, the certificate store is more like a cache. Trust relationships are managed within the PKI, which is a separate entity. 

`sq` tries to minimize user interaction (for instance, in the form of yes/no questions). The design of the user interface aims to make these kind of questions superfluous. 

`sq` uses subcommands to structure its interface. It has `help`-pages for each subcommand in the hierarchy to help with orientation. Alternatively TAB expansion gives you a first glimpse (and avoids typing long option names or subcommands). 

`sq` is stateful. It operates on its stores and updates them according to the used operations. While there are ways to avoid these updates and work in a stateless fashion, it's not the default mode of operation. If you are looking for statelessness, have a look at [`sqop`](https://gitlab.com/sequoia-pgp/sequoia-sop).

