## How to revoke a certificate

To revoke a certificate, use 'sq key revoke' followed by `<REASON>` and `<MESSAGE>`:


```shell
$ sq key revoke --cert $FINGERPRINT --reason superseded --message 'there will be a new one'
```


To revoke a USER ID from a certificate, add the subcommand `userid`:


```shell
$ sq key userid revoke --cert $FINGERPRINT --userid 'alice' --reason retired --message 'testing_purposes'
```


> The process is explained in more detail in this document: [Chapter 'Revocation'](./revocation.md)
