## Hardware keys/ Smart cards

Hardware keys or smart cards are small computers. They come as USB sticks, in credit card format or as a SIM card (and probably other form factors). Their main purpose is to protect secret key material. Secret key material can be written to and stored on these devices, but never read. Instead, these devices can be requested to sign or decrypt data, usually after supplying some kind of authentication (like a PIN). That way secret key material can be used without disclosure.

`Sequoia PGP` (and therefore `sq`) uses key- and cert stores to manage the respected objects. These stores delegate requests to backends, one of these backends is `gpg-agent`. As long as `gpg-agent` can use your openpgp hardware token, `sq` can do so as well.

Please note that if you point the environment variable `SEQUOIA_HOME` to a different location than `default`, `gpg-agent` support is disabled.

