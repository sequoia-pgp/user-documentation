## Certificate management

### Listing available certificates

Certificates are usually stored in the cert store. To list the content of this store use:

```shell
$ sq cert list
```

This command outputs all certificates in the store which are **authenticated**. `sq` only uses authenticated certificates for cryptographic operations. 

A list of all usable certificates in the store is available via:

```shell
$ sq cert list --gossip
```

Usable is this context means, that these certificates are authenticated or can be authenticated if needed. This list does not include revoked or expired certificates. If you want to see them as well, use `--gossip` with `--unusable`:

```shell
$ sq cert list --gossip --unusable
```

The list of certificates can be filtered by:

```shell
$ sq cert list $PATTERN
```

In this case, `$PATTERN` is just a string (in UTF-8). Each certificate which contains a User ID containing this string will be displayed.

```shell
$ sq cert list bob@example
 - 61E064A38CD0230666D40B823CB405E519E599A1
   - created 2024-10-23 09:08:22 UTC
   - will expire on 2027-10-24 02:34:43 UTC

   - [    ✓    ] <bob@example.com>

Hint: To view why a user ID is considered valid, pass `--show-paths`.

Hint: To see more details about a certificate, for example 61E064A38CD0230666D40B823CB405E519E599A1,
      run:

  $ sq inspect --cert=61E064A38CD0230666D40B823CB405E519E599A1
```

This is an example where `$PATTERN` contains an incomplete email address, which matches one certificate.

### Paths

Certificates are considered authenticated if there is a path from the "Local Trust Root" certificate. Use `--show-paths` to display existing paths:

```shell
$ sq cert list bob@example --show-paths
 - 259392E02F8DCC447ED77A3114A33DD1C6BD5288
   - created 2024-11-07 11:33:24 UTC
   - will expire 2027-11-08T04:59:45Z

   - [    ✓    ] <bob@example.com>

     ◯─┬ 686D08D93C582976252805A6695224B3564396B2
     │ └ ("Local Trust Root")
     │
     │  certified the following binding on 2024‑11‑07 as a meta-introducer (depth: unconstrained)
     │
     └─┬ 259392E02F8DCC447ED77A3114A33DD1C6BD5288
       └ "<bob@example.com>"


Hint: To see more details about a certificate, for example 259392E02F8DCC447ED77A3114A33DD1C6BD5288,
      run:

  $ sq inspect --cert=259392E02F8DCC447ED77A3114A33DD1C6BD5288
```

This example shows that `Local Trust Root` directly certified the binding of `bob@example.com` with its certificate, when this certification was made and the assigned trust depth.

### Finding certificates in the local cert store

There are further commands `sq` implements, which are more focused on specific operations on authenticated (directly usable) certificates.

- `sq pki identify --cert $FPR` shows the certificate `$FPR` like `sq cert list $FPR` does. The differences is that `sq pki identify` will not try to use `$FPR` to match User IDs.
- `sq pki lookup` will explicitly interpret its parameters as User IDs. There are `--userid` and `--email` as designators available.
- `sq pki authenticate` takes a certificate and a User ID and checks if there is a binding between these in the cert store, which is authenticated.

These three commands can also be used on unauthentic bindings, if you pass `--gossip`.

```shell
$ sq pki lookup --email alice@example.com --gossip
```

### Modifying a certificate

If you want to modify or add a User ID or update expiration dates, you have to use the corresponding key instead. User IDs and metadata like expiration dates are (self)signed by the primary key and the necessary secret key material is not part of a certificate.

After modifying your key, you have to export the certificate again. It will now contain the modifications applied to the key and can be published in the usual way.

Along the same line, certificates cannot be revoked, this also has to be done using the key.

There are ways to modify a certificate, for instance by adding a certification - but these modifications will not be self signed. The self signature is used to show that the signed component in a certificate is aligned with the intention of the keyholder.
