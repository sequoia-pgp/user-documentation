## Using subkeys 

Subkeys are key pairs, but associated with the primary key. There are differences between Sequoia PGP and other OpenPGP implementations in how keys are structured by default when they are generated. 'sq' generates a primary key for certification and respectively subkeys for authentication, signing and encryption, even though it's possible to generate more subkeys in case of need. Fire more technical information see chapter [Keys and certificates](./bckgrnd_keys_certificates.md).

The primary key should be the most protected one and therefore stored offline where no one can have access to it.
For this reason, the best practice would be not to publish the primary certificate. Instead it makes sense only to publish certificates of subkeys, to avoid that other people use the cert of the primary key. Again, this is not a security consideration, but if people use the primary certificate, it will become difficult to keep the primary key offline.

In this scenarios subkeys are the ones to use for frequent work. Then you just need to publish the certificates of the respective subkeys. It makes sense to rotate them more often by revoking them faster and let them expire earlier than the primary key. The primary key can be used to revoke a subkey if it's needed.

To prevent losing data if an encryption subkey gets lost, it's necessary to have a backup of it in a safe place.  
In case of a signing key, it is not as important or even not recommended to have a backup file. This is simply to minimize the risk of key theft by avoiding storing it in two different places. In case of loss the functionality will not be limited, signatures made previously with that key still remain valid and for future signatures you can easily generate a new one using the primary key.

Just a site note, in some countries there is even more a need to separate keys for encryption and signing in terms of key disclosure laws. For example in the UK and in France it can be construed as a crime not to decrypt and/or hand out a decryption key to law enforcement agencies if recommended, even without a court order. In this cases the rest of the key material should remain safe.

<!-- How to use subkeys, especially how to use them when the primary key is offlined. `sq key subkey export...`
- https://openpgp.dev/book/certificates.html#component-keys
- different subkeys for different devices -->

