## How to approve a certification

To approve a certification which has been received by a third party, a keyholder can approve it by using the following command:

```shell
$ sq key approvals update --all --cert $FP_KEYHOLDER
```

> Find more information here: [Chapter 'Certify and authorize certificates'](./cert_certify.md).



