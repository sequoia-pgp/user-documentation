## Certificate Authorities

Imagine an NGO which wants to attest that the certificates of its staff members are authentic - that they indeed work for this NGO and that the User IDs of these certificates are the right ones, are actual representations of staff members. In other words: The NGO wants to establish an authority which can be queried about the validity of a certificate.

While there are many ways to do so, the attestations should be processable by an algorithm - human involvement should not be necessary. Additionally the authority shouldn't be localized to avoid a single point of failure. A server in the internet which judges uploaded certificates is therefore not a solution for this recommendation. 

A solution would be to create an additional key whose sole purpose is to certify the staff members certificates by adding its signature to them. An interested user could then search for this signature in a certificate to verify its validity.

However, given that anyone can create a key with the same User ID as the special signing key above and then sign certificates with it, the problem just got shifted. Instead of authenticating a staff member's certificate, you have to verify the authenticity of the signing key. This however introduces a beneficial indirection - members to the staff can leave, new ones can enter - as the signing key stays the same, no further authentications are necessary for an outside user.

The signing key plays the role of a **Certificate Authority** (CA).

Viewed from the outside, such a setting of a CA key and signed certificates is just a bundle of entangled certificates. This bundle becomes useful when the CA key (or more precise: its certificate) is used as a trust introducer. 

<!-- [comment > limit/restrict the trust introducer. Should that be here, or in an other chapter?] -->

Everyone can create a CA, it's not limited to established institutions. But usually the term CA denotes some degree of institutionalization.  

The certificate of a CA can itself be certified by other keys - either by (other) CAs or individuals.

And that's it: a Certificate Authority is just a key like any other, but used in a specific way. 

## Shadow CAs

The certificate of a CA key has to be publicly available to serve its role. 

The principle of delegation, the reliance on the authenticity of a binding to a designated key, can be used locally too. This is what Sequoia PGP (and therefore `sq`) does. 

When first invoked, `sq` creates a **Local Trust Root**, that is a key located in the local cert store which certifies other certificates. 

A certificate - or more precise: its User ID - is considered authenticated if there is a path from the Local Trust Root to this User ID of the certificate with a trust amount of 100% (or 120 out of 120).

`sq` further creates more (intermediate) CAs on demand. Their role is a record of the provenience of a certificate and a trust amount derived from that provenience.

As an example: If you fetch the certificate of `alice@example.com` from the keyserver `keys.openpgp.org`, `sq` will create two internal CAs. One named `Public Directories` and one named `Downloaded from keys.openpgp.org`. It will then create a path `Local Trust Root -> Public Directories -> Downloaded from keys.openpgp.org -> alice@example.com`.

Along that path a trust amount is calculated: the internal CA `Public Directories` for instance is certified with a trust amount of 40/120. This amount creates an upper limit for the whole path, other CAs in that path can further reduce that amount, but cannot increase it. `Downloaded from keys.openpgp.org` is certified by `Public Directories` with a trust amount of 1/120. The complete path from `Local Trust Root` to `alice@example.com` has therefore a combined trust amount of 1/120.

To increase this value, you could change the certifications of the internal CAs by creating new ones - or (the recommended way) by creating a new path between `Local Trust Root` and `alice@example.com` by using `sq pki link add` - linking `alice@example.com` and `Local Trust Root`. 

<!-- [comment See chapter (TODO: add link) for an indepth explanation.] -->

Besides calculating a trust amount, the path also shows from where a certificate originates and when it was included in the cert store, as certifications have a creation date.

These internal CAs are called **Shadow CAs**.

