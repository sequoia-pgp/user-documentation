## Encrypt and decrypt a file

### Encryption

`sq encrypt` takes data from a file or from `STDIN` and encrypts it, using key material from certificates passed to it.

If you encrypt a file for someone else's User ID and want to be able to read it afterwards, you have to also add your own User ID.

Certificates for encryption - the *recipients* - can be selected by:
- `--for $FINGERPRINT` - select the certificate identified by `$FINGERPRINT`
- `--for-email $EMAIL` - select **all** certificates with User IDs containing `$EMAIL`
- `--for-userid $USERID` - select **all** certificates with User ID `$USERID`
- `--for-file $FILE` - use the certificates in `$FILE`
- `--for-self` - uses certificate(s) specified in the configuration file under `encrypt.for-self`

One thing to keep in mind: If you use `--for-email` or `--for-userid`, `sq` only considers certificates which are *authenticated*. If you want to use an unauthenticated certificate, you can use the fingerprint as selector (as fingerprints are self-authenticating) or `--for-file`.   

By default `sq encrypt` signs encrypted messaged. The key to use for signing can be passed by:
- `--signer $FINGERPRINT` - use the key identified by its fingerprint
- `--signer-email $EMAIL` - use all keys with User ID `$EMAIL`
- `--signer-userid $USERID` - use all keys with User ID `$USERID`
- `--signer-file $FILE` - use key contained in `$FILE`
- `--signer-self` - use key specified in the configuration file under `sign.signer-self`
- `--without-signature` - don't sign message

```shell
$ sq encrypt --for-email alice@example.com message.txt --signer-email bob@example.com --output message.pgp
```

This encrypts the file `message.txt` using any certificate containing the email `alice@example.com`, the result is written to `message.pgp`. Without `--output` the encrypted file is printed to `STDOUT`.

```shell
$ sq encrypt --for $FINGERPRINT message.txt --signer-email bob@example.com --output message.pgp
```
Does the same, but selects the certificate used for encryption by its fingerprint.

You can create an encrypted file using just a password by providing `--with-password` - `sq` will prompt you for the password. 

All this can be combined:

```shell
$ sq encrypt --for $FINGERPRINT \
    --for-email alice@example.com \
    --for-userid "Bob Example" \
    --with-password \
    --without-signature \
    message.txt --output message.pgp
```

The input - the message to encrypt - does not have to be in a file. If the file is missing in parameter list, the message is taken from `STDIN`.

```shell
$ echo "Hello world" | sq encrypt --for $FINGERPRINT --without-signature
-----BEGIN PGP MESSAGE-----

wV4D+zMBYd4zQtASAQdAM/WW6LvAEEc7SdDEYgo0s38DtywJEB5A8XIt1JhzbTcw
WMqpUI3xbb4ZBqWK9R8/DyIAOqAO1rH55vkdU63OTkj4WKo6f6c8lfMxD8JvYaGV
0j0BMEm+mp706Kpg2Ac/f3Hdn9IHb+jbeCUH/Rem2y+Wr9PrOPyL6vc1MFhCTrd+
9a2XDB3avQcYruJBSxmL
=IX5I
-----END PGP MESSAGE-----
```


To encrypt a message for yourself, you need to set the recipient(s) in the configuration file beforehand to enable `--for-self` to apply it:

```shell
$ mkdir -p ~/.config/sequoia/sq/
$ sq config template --output ~/.config/sequoia/sq/config.toml
$ $EDITOR ~/.config/sequoia/sq/config.toml
[...]

[encrypt]
for-self = ["1C88780EF239586AF463758094119887B3462B88"]
[...]
[sign]
signer-self = ["1C88780EF239586AF463758094119887B3462B88"]

[...]

$ sq encrypt --for-self --signer-self message.txt
```

For more information about the configuration file see chapter [configuration](./configuration.md).

#### Using v6 (RFC9580)

When encrypting, the used parameters are taken from the certificate - this includes the format of the encryption container: If the certificate is of version v4, `sq` will use a corresponding container. However if there is no guidance - for instance when just using `--with-password` - `sq` can be directed to use a specific encryption container format by passing `--profile`.

The values of `--profile` can be `rfc4880`, which uses a v4 container, or `rfc9580` which uses a v6 container.


### Decryption

Decrypting an encrypted file and writing it to a file works as follows:

```shell
$ sq decrypt message.pgp --output message.txt
```

As the encrypted message (usually) contains the ids used during encryption, decryption needs no further help to select the right key. To have `sq decrypt` sending its output to `STDOUT`, just leave out the `--output` parameter.



