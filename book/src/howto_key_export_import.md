## How to import and export a key


### Import a key into keystore


```
$ sq key import $KEYFILE
```


### Export a key from the keystore into a file


```
$ sq key export --cert $FINGERPRINT > $KEYFILE
```


      
> See chapter ['Import and export of a key'](./key_export_import.md) for more detailed information.
