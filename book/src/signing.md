## Signing files and messages

Signing files and/or messages means creating a signature over data. There are two ways to store the newly created signature:

- by wrapping the file in an OpenPGP message structure which includes the signature
- by creating a detached signature within its own file, leaving the signed data untouched

The first option has the advantage that everything is in one file. The advantage of the second option is that the signed file doesn't change, so that it can be used without unwrapping the OpenPGP message structure first.

### Creating an inlined signature

```shell
$ sq sign --message --signer $FINGERPRINT $FILE
```

In this example a signature is created over the content of `$FILE` file using the key designated by `$FINGERPRINT`. Instead of using the data from a file, `sq sign` can also take data from `STDIN`.

```shell
$ echo "my message" | sq sign --message --signer $FINGERPRINT
-----BEGIN PGP MESSAGE-----

xA0DAAoWQeEtk/c7kG4Byw9iAAAAAABleGFtcGx0ZQrCvQQAFgoAbwWCZyt6wAkQ
QeEtk/c7kG5HFAAAAAAAHgAgc2FsdEBub3RhdGlvbnMuc2VxdW9pYS1wZ3Aub3Jn
72xkqs1135T2u5oIlngq51GwLmFm8jevOh4nssMT/WMWIQTlcn7y4+W/syeiLW1B
4S2T9zuQbgAA4VgBAK5XsyCTIA1VrZQYkKm7BpygYnco7K+IrWFR9ePczM3BAP9j
6V37oWwULdWG3vZsIweDNbjWfHeblOQzajAFjDEWDA==
=HkKe
-----END PGP MESSAGE-----
```
The output of this operation is printed to `STDOUT`.

These examples generated an 'inline' signature, the output isn't human readable. To generate a clear text signature, exchange `--message` with `--cleartext`.


```shell
$ echo "my message" | sq sign --cleartext --signer $FINGERPRINT
-----BEGIN PGP SIGNED MESSAGE-----
Hash: SHA512

my message

-----BEGIN PGP SIGNATURE-----

wr0EARYKAG8FgmdiuRoJEA7sKm178RIgRxQAAAAAAB4AIHNhbHRAbm90YXRpb25z
LnNlcXVvaWEtcGdwLm9yZ/iaMdUKZbOpbMwwl+oThAXY3MMiAfXrjjKYsdoa+7M5
FiEE6QB8bZ+zNxhjUeC/DuwqbXvxEiAAAK3aAP96QhpFy782+306HDPMtaGOCNQq
fVag1Bsl0aGByI3r0wD/TPI5Md89V0ly+ixQ6SAUKgKONHEgkgaZ3sfUaCf4qAc=
=DZUO
-----END PGP SIGNATURE-----
```

Instead of using a key from the keystore (via `--signer`), a file containing a key can be used: `--signer-file $KEYFILE`.

### Creating a detached signature

```shell
$ sq sign --signer $FINGERPRINT --signature-file $SIG_FILE $FILE  
```
This example creates `$SIG_FILE` containing the detached signature. It throws an error if `$SIG_FILE` existed before. You can add `--overwrite` in this case.
