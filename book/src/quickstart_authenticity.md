## Quick start

This is an overview over operations concerning authenticity. It is by no means complete, but might give you a first orientation.

### Getting a certificate from the internet

```shell
$ sq network search $QUERY
```

Search default keyservers, WKDs and DNS servers for certificates with User IDs or fingerprints matching `$QUERY`.

### Publish a certificate

```shell
$ sq network keyserver publish --cert $FINGERPRINT
```

Publish the certificate with fingerprint `$FINGERPRINT` on the default keyservers.

### Authenticate a certificate

`sq` only uses authenticated certificates. That means that certificates can only be designated by a User ID if the binding between this User ID and a certificate is marked as valid. This doesn't apply if you select a certificate by fingerprint (or use from a file), as fingerprints are regarded as self-authenticated. To mark a binding as authenticated, use:

```shell
$ sq pki link add --cert $FINGERPRINT --userid $USERID
```

This only authenticates this specific User ID, other User IDs of the certificate are not effected. Use:

```shell
$ sq pki link add --cert $FINGERPRINT --all
```
as a convenience to authenticate all User IDs in a certificate in one go. 


### Retract authentication from a certificate

To reverse an authentication use:

```shell
$ sq pki link retract --cert $FINGERPRINT --userid $USERID
```

or the following to retract any associated links of a certificate:

```shell
$ sq pki link retract --cert $FINGERPRINT
```

### Import a certificate from a file

```shell
$ sq cert import $FILE
```

### Export a certificate to a file

```shell
$ sq cert export --cert $FINGERPRINT
```

### Certifying a certificate

also known as "signing a key":

```shell
$ sq pki vouch certify --certifier $MY_FINGERPRINT --cert $OTHER_FINGERPRINT --userid $USERID
```

This way the binding between `$OTHER_FINGERPRINT` and `$USERID` is certified as authentic by the certificate `$MY_FINGERPRINT`. Don't forget to publish your newly created certification.

### Approving a certification

To avoid flooding of certificates with bogus certifications, keyservers might insist on an approval by the keyholder of the certified certificate. To approve all pending certifications:

```shell
$ sq key approvals update --add-all --cert $FINGERPRINT
```

### Verify a data signature

To verify a detached signature, use:

```shell
$ sq verify --signature-file $DETACHED_SIGNATURE_FILE $FILE
```

To verify a signed message use:

```shell
$ sq verify $FILE
```

Please note that for a successful verification the signing certificate has to be authenticated.

### Sign files and messages

```shell
$ sq sign --signer $FINGERPRINT $FILE
```

Sign a file using the key with fingerprint `$FINGERPRINT`.

### Sign with detached signature

```shell
$ sq sign --signature-file $DETACHED_SIGNATURE_FILE --signer $FINGERPRINT $FILE
```
