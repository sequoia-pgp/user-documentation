# Confidentiality

This chapter deals with subjects related to confidentiality. It contains the generation of keys and maintaining certificates. It's also about using the key material for encrypting and decryption files as well as publication and revocation of certificates.






