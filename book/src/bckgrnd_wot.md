## Web of Trust


There are relationships between keys, certificates, signatures, etc. which are computable. Given a file, a detached signature and a certificate, one can calculate if the signature is made over the file using the key corresponding to the certificate. There is no other (outside) information, no context needed to validate this relationship.

Cryptographic keys and certificates are rarely (if at all) used without context. A context which comes to mind would be email encryption. This context introduces a new set of entities, namely email addresses. There is no way to derive the key from the email address alone - there is no computable relationship. For that a mapping is needed, which binds an email address to a key (and vice versa). 

By definition this mapping is arbitrary and not computable - otherwise it wouldn't be needed. Technically any email address can bind to any key. In that sense a binding is no more than a claim.

This introduces the problem of authenticity: is the claimed binding correct, is the holder of the key also owner of that email address?
As this problem cannot be computably solved, it has to be addressed by other means: human intervention - also known as "work".

Authenticity can only be established by humans.

This is not a good requirement, since humans tend to avoid work. This leads to keys being used without proper authentication, signatures not being checked and so on.

If human work cannot be eliminated from that process, it makes sense to use the result of that work in the most efficient way.

Probably the first optimization of this process which comes to mind is the avoidance of repeating work. For this the results of work (of authenticating a binding) has to be captured in a persistent form which can be stored and reused. 
OpenPGP realizes this by creating a signature, where the person doing the work signs the authenticated binding. Next time a key lookup is done, the signature will be recognized and the binding will be taken as authentic - which is a process that happens without human intervention.

The signature as a result of work can be seen as a product. This product can be passed on to others, so that they too can benefit from the work inside it.
  
This approach is not limited to OpenPGP, x509 operates along the same idea where some authority attests authenticity and spreads this attestation.

This gives rise to a new problem: product quality. The signature gives no clue on how diligent the authentication was performed, if at all. 
 
There is no good solution to this problem. Unless repeating the authentication and comparing the result, one can only rely on the reputation of the individual (or institution) which made the attestation.
OpenPGP supports this reputation model by formalizing a way to attest good reputation - good work. This is done by assigning a trust depth while certifying the authenticity of a certificate / User ID binding. A trust depth of one for instance means that the attestation of authenticity not only covers the actual binding, but also attestations made by the certificate of that binding - the attestation spreads out one "hop". Higher levels of trust depth simply increase the number of hops an attestation covers.

Accompanying the outreach of a reputation, there is also the possibility to quantify it. Instead of attesting only a good reputation (or not), OpenPGP allows to assign a value between 0 and 120, expressing the trust in an attestation made by a certain keyholder. Such an attestation would then be viewed as "partially authenticated". The values can add up, if independent sources only express limited trust in a reputation, so that the result might qualify as "good enough" (for a certain use case).

So far a reputation can be characterized by its outreach and its (assumed) quality - the diligence put into the product. This can be further tuned by limiting the attestations to a certain range of User IDs. That way only the attestations matching a supplied regular expression would be considered. A typical use case would be a certification authority of an institution, where only the attestations for User IDs from that institution (having a specific mail domain) would benefit from the assigned outreach and trust amount. This doesn't stop a CA from attesting User IDs from other domains, but those attestations would be ignored.

Attestations are bundled with the corresponding certificates and can be circulated the same way the original certificate circulated. The way a certificate circulates might give hints on its authenticity. This is usually done by using a communication channel which involves control over a certain resource linked to the User ID part of a binding. Using DANE for instance implies control over the DNS zone file of the mail domain. WKD implies control over a webserver answering on a domain linked to the mail domain. Other proofs of control might also give clues on the authenticity of a binding.

<!-- > TODO: TOFU: Turning refusal of work into a feature. TOFU as a special "proof of control" as proof of stability -->

So far this situation is not yet a web of trust - it's more a soup of partially entangled certificates. In some sense this soup is result of the collective work of OpenPGP users, it's kind of a commons.

It gives you a glimpse on how others judge the authenticity of bindings, which certificates belong to a person with good reputation and so on. But these are judgments of other people. To benefit from the work contained in this soup one has to start using the products. This is done by attesting authenticity and trust depth to certificates from this soup - obviously not arbitrarily, but after checking. These certificates become personal trust anchors (or introducers) and are the starting point of a web of trust.

A web of trust is the result of personal choice thus differs between different users. This is in stark contrast to x509 where the trust anchors are defined by institutions - web browser vendors for instance.

