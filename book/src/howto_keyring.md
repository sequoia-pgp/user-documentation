## How to manage keyrings



### Creates a keyring by exporting certificates from the cert store:

```shell
$ sq cert export --cert $FPR_A --cert $FPR_B --cert $FPR_C ... > $FILE
```


### Lists the certificates in a keyring:


```shell
$ sq keyring list example_keyring.pgp
0. 159F7D1D498EF2BFC489D13FB5A86AAD248300C8 Bob
1. 722E3D4101B82C0AE80A3EF07A512226FF0D14E7 Alice
2. 8B19816EE960EEBBD09A1256CF755B2FB77C6171 Dave
[...]
```

### Extracts certificates from a keyring:

```shell
$ sq keyring filter --experimental --userid Alice example_keyring.pgp
-----BEGIN PGP PUBLIC KEY BLOCK-----

xjMEZ7Wz3hYJKwYBBAHaRw8BAQdAuhfXxD0ewpB0BrkDpMH5YhmobPkp/eGpJRxu
[...]
=adZu
-----END PGP PUBLIC KEY BLOCK-----
```


### Splits up a keyring into its certificates:

```shell
$ sq keyring split $FILE
```


### Merges a split keyring:

```shell
$ sq keyring merge *
-----BEGIN PGP PUBLIC KEY BLOCK-----

xjMEZ7Wz3hYJKwYBBAHaRw8BAQdAuhfXxD0ewpB0BrkDpMH5YhmobPkp/eGpJRxu
[...]
=MuLv
-----END PGP PUBLIC KEY BLOCK-----
```



More on managing [keyrings](keyring.md). 
