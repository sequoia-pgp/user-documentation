## About this guide

This guide is about `sq`, the command line interface to Sequoia PGP. Here you will find an overview of the most common use cases and commands. There are also chapters about encryption in general as well as underlying concepts of Sequoia PGP in particular. 

Very specific examples are presented only occasionally, if you can't find anything about a specific case here, please take a look at the respective help- or man page of the command or subcommand. The same applies if some details are out of date. **This book is under constant revision** and we try to keep up with the fast development of sq. It is built from the ['sequoia-pgp/user-documentation' git repository](https://gitlab.com/sequoia-pgp/user-documentation), where changes are made and tracked.

