## Conventions, type setting and license


### Suffixes

This guide uses the following filename suffixes:

- certificates: .cert
- keys: .key
- encrypted messages: .pgp
- clear text messages: .txt
- detached signatures: .sig
- revocation certificates: .rev


### Type settings

We present “typescripts” of command line use like this:

```shell
$ sq version
sq 1.3.0
using sequoia-openpgp 2.0.0
with cryptographic backend Nettle 3.8 (Cv448: true, OCB: false)
```


The first line is the shell command. The $ represents the shell prompt: the dollar sign is traditional for Unix, but it’s likely that your actual prompt is different. The rest of the line is the command you write to invoke a command. The rest of the typescript is the output of the command. A typescript may contain multiple commands, and are all identified by a leading dollar sign.

### Copyright license

<!--
Copyright 2021 The pep foundation
-->
This guide is licensed under the Creative Commons Attribution-ShareAlike (CC-BY-SA) 4.0 International license. It is based on https://creativecommons.org/licenses/by-sa/4.0/legalcode.txt, but typeset via Markdown, using commit b1a347d40d2a50ed345a9b152248cca6b9f0f803 of Creative-Commons-Markdown.git. It matches the plain text version, linked above, except for typesetting. In case of differences, the plain text version is authoritative.

