## Maintaining a key

It's possible to change certain settings of already created key material, such as updating the expiration time or adding a User ID. Others like for example the cipher suite are set once and can't be updated later.


### Adding a User ID

If you want to add a User ID like an additional name and/or another email address to a certificate, `sq key userid add` is the subcommand to use:


```shell
$ sq key userid add --cert $FINGERPRINT --email alice@work.example.com --name alice_at_work
```

> Note that if you accidentally specified and published a wrong User ID and want to remove it, you have to revoke the certificate. There is no other way to get rid of it, as stripping a User ID only works on locally stored certificates, not on certificates on keyservers. 



#### Change expiration date

To change the expiration time, you can use the `sq key expire` subcommand:

```shell
$ sq key expire 2y --cert $FINGERPRINT
```

Note: This command does not change the associated subkeys.


If you instead or additionally want to change the expiration of a subkey, choose the `sq key subkey expire` subcommand as follows. Note that --key requires the fingerprint of the subkey you want to change and `--cert` the fingerprint of your primary certificate.


```shell
$ sq key subkey expire never --key $SUBKEY_FINGERPRINT --cert $FINGERPRINT
```

