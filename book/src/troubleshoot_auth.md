## Troubleshooting regarding authenticity issues


### Can't find a specific certificate in my cert store

If you use `sq cert list` - even with the option `--cert-email=<EMAIL>` - you sometimes find yourself in a situation where you don't get what your looking for. Some certificates may not be displayed because they are either unauthenticated or 'unusable'. 'Unusable' means either 'expired', 'revoked' or not conforming to the configured policy (like using deprecated hash algorithms). If so, there will be a hint at the end of the output like this:


```shell
$ sq cert list
[...]
226 bindings found.
Skipped 128 bindings, which are unusable.
Skipped 29 bindings, which could not be authenticated.
Pass `--gossip` to see the unauthenticated bindings.
```

#### Solutions

As proposed in the hint message, the certificates with unauthenticated bindings can by found by using the just used command followed by the`--gossip` option: 

```shell
$ sq cert list --gossip
[...]
226 bindings found.
Skipped 128 bindings, which are unusable.
```

The unusable bindings respectively certificates which are found but not shown can be displayed by using the option `--unusable` in combination with the option `--gossip`, because they are also considered as 'unauthenticated'.

```shell
$ sq cert list --gossip --unusable
```



### A certificate is unauthenticated

If a binding between a User ID and a certificate is unauthenticated, `sq` will refuse to use the certificate, for example to encrypt a file with your own certificate or another one, if you access it via cert store (which is recommended). 

#### Solution

To turn an unauthenticated certificate into an authenticated one, you could - after checking its correctness - mark a binding as authenticated by using the command `sq pki link add` like this:

```shell
$ sq pki link add --cert=EB28F26E2739A4870ECC47726F0073F60FD0CBF0 --email=alice@example.org
```

Alternatively you could  use `sq pki vouch add` to certify the binding with a trust root like so:


```shell
$ sq pki vouch add --certifier=$MY_FINGERPRINT --cert=$ALICES_FINGERPRINT --email=alice@example.org
```

If you are absolutely sure about the authenticity of a certificate's binding, it's also possible to use the certificate directly from file instead from the cert store. Here is an example on how to apply:

```shell
$ sq verify --signer-file alice.cert file.txt                                                                                                   
```                                             


### My certificate is expired

If a certificate is expired, it is obviously also not usable. Here is an example where we try to encrypt a file with our own expired certificate, but the same also applies to messages that shall be encrypted with a third-party certificate:

```shell
$ sq encrypt --for=$FINGERPRINT --signer=$FINGERPRINT file.txt

  Error: $FINGERPRINT was not considered because
         it is: not alive
because: Found no suitable key on $FINGERPRINT
```

Now you can have a closer look into the certificate by using the command `sq inspect` to see what is meant by 'not alive':

```shell
$ sq inspect --cert $FINGERPRINT
OpenPGP Certificate.

      Fingerprint: xxx
                   Invalid: The primary key is not live: Expired on 2023-01-09T16:52:48Z
[...]
```


In this case you could either use another - valid - certificate or make the old key valid again. This also works if the key has already expired.


```shell
$ sq key expire --expiration 1y --cert $FINGERPRINT

Hint: Imported updated cert into the cert store.  To make the update effective, it has to be published so that others can find it, for example using:

  $ sq network keyserver publish --cert=$FINGERPRINT
```


In case you tried to encrypt with a certificate from someone else, you could search for new valid certificate:

```shell
$ sq network search $FINGERPRINT
```

#### A certificate is revoked

If a certificate is revoked, it is suggested to first have a look into the revocation certificate message, which will in the best case announce
a follow-up certificate including a fingerprint. If so, you could search for this specific certificate:


```shell
$ sq network search $FINGERPRINT
```

If there was no announcement, you could still search for a new certificate using for example an email address, keeping in mind that there might be included false results: 

```shell
$ sq network search alice@example.org
```



#### Certificate uses deprecated hash algorithms like SHA1

Finally, if you have a certificate for which you have access to the secret key material and that is considered as 'unusable' because it uses SHA1 signatures, you could update it by using the command `sq cert lint`. For this operation you can find the full instructions [here](./lint.md).
