## Getting started

Let's start with a short overview of some terminology, the toplevel structure of `sq` and some features to support you when using `sq`.

### Terminology

#### Keys and certificates

`sq` uses a different terminology when it comes to certain cryptographic artifacts. What is known as "public key" in other OpenPGP software (like gpg), is called a "**certificate**" in `sq`. "Private keys" are just called "**keys**". For the rational of this renaming see chapter [Background: Keys and certificates](./bckgrnd_keys_certificates.md).


#### Stores instead of keyrings

Some popular OpenPGP softwares store their artifacts in keyrings (secring.pgp, pubring.pgp), `sq` uses stores for that. Keys are stored in the key store, certificates in the cert store. `sq` has (limited) support for keyrings to help (for instance) with migration.

#### "Authenticated"

"Authenticated" in the context of `sq` means: "The right thing". Its main usage is in the selection or designation of a certificate. If you designate a certificate by its fingerprint, this designation is distinct, as fingerprints are unique. Designating by User ID is not, as these cannot be deduced from the key material like a fingerprint. To use the binding between a certificate and a User ID to designate a certificate (for instance to select a certificate by an email address), this binding has to be **authenticated**. It has to be manually marked as valid or inherit authenticity by being certificated by a **trust introducer**.

#### Trust introducer

A trust introducer is a person or institution which is considered as trustworthy (and competent), so that certifications done by the trust introducer are taken over. Trust introducers can be limited in scope, 
- so that only specific certifications are considered, 
- in outreach - can a trust introducer introduce other entities as introducer?
- and trustworthiness, which effects the amount of "authenticity" a certification adds to a specific certificate. 

Technically a trust introducer is represented by a certificate. Each user has to individually promote a certificate to become a trust introducer. There is no limit in the number of trust introducers a user can have.

#### Public Key Infrastructure (PKI) 

`sq` does not support implicit authentication: it's not enough to just import a certificate (aka public key) to use it. You have to authenticate the User IDs in it beforehand - declaring a "trust relationship". 

These relationships are managed within the **PKI**. You can think of it as your local [Web of Trust](./auth_certs.md).

### Structure of the interface

The functionality of `sq` is split into categories. Related functionality is located in the same category, sometimes even in subcategories. 

The top level as orientation:

-  **encrypt**  Encryption of messages or data files.
-  **decrypt**  Decryption of messages or data files.
-  **sign** Signing messages or data files.
-  **verify**   Verify signed messages or detached signatures.
-  **download** Download and authenticate a file in one go.
-  **inspect**  Inspect data, like file(1). Works on files, but also on objects in the different stores like certificates or keys.
-  **cert** Manage certificates. Import, export, list and the like. 
-  **key**  Manage keys. Generating, revoking, listing, import/export, updating metadata like expiry date and more. 
-  **pki**  This is where your "trust management" lives. Authenticating, lookup, trust paths and amounts and more.
-  **network**  Retrieve and publish certificates over the network.
-  **keyring**  Manage collections of keys or certs in files.
-  **packet**   Low-level packet manipulation.
-  **config**   Get configuration options.
- **version**   Detailed version and output version information.
- **help**  Print this message or the help of the given subcommand(s).


For example, you can list your keys with:

```shell
$ sq key list
```

##### Help

`sq` has `help` pages for every command. If in doubt, use:

```shell
$ sq some commmand --help
```

to get a short overview of parameters and functionality of said command. You can also use:

```shell
$ sq help some command
```

##### TAB completion

`sq` comes with TAB completion:

```shell
$ sq command <TAB><TAB>
```

shows you possible continuations of the command: available subcommands, parameters and other options. Likewise `sq com<TAB><TAB>` will expand to `sq command `.


##### Hints

After completing a command, `sq` prints some hints for possible follow up commands:

```shell
$ sq key generate --own-key --email 'alice@example.com'
[...]

Hint: Because you supplied the `--own-key` flag, the user IDs on this key have been marked as authenticated, and this key has been marked as a fully
      trusted introducer.  If that was a mistake, you can undo that with:

  $ sq pki link retract --cert=0F761BE91983E9B50F2180CA8747303171399A43 --all

Hint: You can export your certificate as follows:

  $ sq cert export --cert=0F761BE91983E9B50F2180CA8747303171399A43

Hint: Once you are happy you can upload it to public directories using:

  $ sq network keyserver publish --cert=0F761BE91983E9B50F2180CA8747303171399A43
```

> Please note that the hints pick up the results of the command and include them appropriately - in this case the displayed fingerprint is indeed the fingerprint of the key just generated.


Named parameters can be written like `--email alice@example.com` or `--email=alice@example.com`, allowing a `=` in between.
