## How to verify a file or a detached signature


### Inlined and cleartext signatures 


Inlined and cleartext signatures can be verified by:

```shell
$ sq verify --message $FILE
```

or

```shell
$ sq verify --cleartext $FILE
```



If the certificate is available, but the signature could not be authenticated, you can either pass the fingerprint of a certificate (aussuming the authenticity of that cert):

```shell
$ sq verify --signer $FINGERPRINT $FILE
```


or can authenticate (at least) one of the User IDs of the certificate by adding a link: 

```shell
$ sq pki link add --cert=$FINGERPRINT --userid=$USER_ID
```

Then verify again.




### Detached signatures


Detached signatures can be verified by explicitly passing the file containing the detached signature:

```shell
$ sq verify --signature-file $SIGNATURE_FILE $FILE
```


See more options and a detailed example in Chapter ['Verify signatures'](./verify.md)


