
## How to generate a key

To generate a key with `sq`, enter the following command in your command line: 


```shell
$ sq key generate --own-key --name alice --email alice@example.com 
```



> To learn about the possibilities and options around generating a key see [chapter 'Key generation'](sq_key_generation.md).
