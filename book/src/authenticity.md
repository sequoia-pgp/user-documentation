## Authenticity

This chapter covers certificates, especially their application in the realm of authenticity. Creating and verifying certifications and signature, revocation of certificates, publish and retrieving and other aspects of managing certificates.  

