## Certify and authorize certificates


### Certifying a User ID for a certificate

While `sq pki link add` generates a signature and adds it to the certificate, that signature is not exportable. `sq cert export` will skip this signature. If you want to share your attestation, you have to generate an exportable signature by using `sq pki vouch certify`.

Other than `sq pki link add` the key which creates the signature when doing `sq pki vouch certify` has to be passed as a parameter. The originator of the certification is an important information for others to evaluate its usefulness. 

A certification is generated as follows, assuming Alice's certificate fingerprint is `FBF875C726E742953B265D8F74704F2E76BE2F9D` and Bob's is `0AF31DA1380F66AB2600BBECF43AFD52BB107C5D`:

```shell
$ sq pki vouch add --certifier FBF875C726E742953B265D8F74704F2E76BE2F9D --cert 0AF31DA1380F66AB2600BBECF43AFD52BB107C5D --email bob@example.com
 - ┌ 0AF31DA1380F66AB2600BBECF43AFD52BB107C5D
   └ <bob@example.com>
   - certification created

Hint: Imported updated cert into the cert store.  To make the update effective, it has to be published so that others can find it, for example using:

  $ sq network keyserver publish --cert=0AF31DA1380F66AB2600BBECF43AFD52BB107C5D
```

With
- `--certifier $FPR_CERTIFIER` being the fingerprint of the originator of the attestation
- `--cert $FPR` the fingerprint of the certificate to attest
- `--email $EMAIL` use the email address to identify the binding which should be certified (you can also use `--all`)

This basically says that alice@example.com is certain that the binding between `$FPR` and `bob@example.com` is authentic and is willing to make that attestation public. Keep in mind that publishing an attestation is an explicit step, generating a certification does not include its publication.

`sq pki vouch add` prints the resulting certificate (the original certificate with the newly created signature) to `STDOUT` or to a file specified by `--output FILE`. The modified certificate is also stored in the local certificate store and can be exported at a later time.

The certification can contain additional information or restrictions:

- A certification created like above has a lifetime of 5 years, after that period the certification is considered as expired. Add `--expiration DATE` or even `--expiration "never"` to change this behavior. The DATE has to be in ISO 8601 format, like `2024-07-02` or a time interval like `3y` (3 years). 


The certificate with the new certification in it can now be disseminated - this however is not recommended. Instead send the new certificate to the respective keyholder, so that he/she can approve your attestation, basically signing your certification. See more details after the following paragraph.


### Approving a certification

This sounds complicated, but it prevents attackers (or trolls) to simply add loads of certifications to a certificate, blowing it up to a size which cannot be handled anymore. For this reason, modern keyservers will filter out certifications which do not have an approval signature.

After receiving newly certified certificate, a keyholder can check that certification by saving it into a file and then inspect, import and subsequently approve that certification as follows:

```shell
$ sq inspect alice.cert  --certifications
$ sq cert import alice.cert
$ sq key approvals update --all --cert $FINGERPRINT
```

This will add an approval signature to all certifications in the certificate identified by `$FINGERPRINT`. Use `--cert-file $FILE` in case you have the certificate as a file and don't want to import it.

After that step the keyholder has to publish that certificate again to make it available to others.


### Querying for certifications

`sq pki vouch list` allows you to get information about certificates in your local cert store.

For getting a list a certifications made by a specific key - for instance a certificate authority - use:

```shell
$ sq pki vouch list --certifier FBF875C726E742953B265D8F74704F2E76BE2F9D
 - ┌ 0AF31DA1380F66AB2600BBECF43AFD52BB107C5D
   └ <bob@example.com>
       - created at 2025‑01‑28 15:10:29
       - expiration: 2035‑01‑29
```

For the other direction - Given a certificate, list those certificates which certified it:

```shell
$ sq pki vouch list --cert 0AF31DA1380F66AB2600BBECF43AFD52BB107C5D
 - Certifier:
   - ┌ FBF875C726E742953B265D8F74704F2E76BE2F9D
     └ Alice (authenticated)

   - Certified the binding:
     - ┌ 0AF31DA1380F66AB2600BBECF43AFD52BB107C5D
       └ <bob@example.com>

       - created at 2025‑01‑28 15:10:29
       - expiration: 2035‑01‑29
```

### Authorize a certificate as a trusted introducer

A trusted introducer is the equivalent of a certificate authority (CA). It expresses the certifiers trust in the certifications made by the trusted introducer / the CA to be valid. Such a certification as trust introducer has a default trust depth of 1, which can be modified by passing `--depth NUMBER` or even `--unconstrained`, which doesn't impose a limit to the depth. This is one way to restrict the depth of that certification by limiting the ability of the trust introducer to specify further introducers. Additionally such a certification can be limited to one or more domains. The trust introducer can create certifications for all kinds of domains, the before mentioned certification will restrict its effect to the passed domains.

A certification to mark a certificate as a trusted introducer is done like this: 

```shell
$ sq pki vouch authorize --certifier $FPR_CERTIFIER --cert $FPR --domain=example.com --email bob@example.com

Certifying "<bob@example.com>" for 1C88780EF239586AF463758094119887B3462B88.


Hint: Imported updated cert into the cert store.  To make the update effective, it has to be published so that others can find it, for example using:

  $ sq network keyserver publish --cert=1C88780EF239586AF463758094119887B3462B88
```

