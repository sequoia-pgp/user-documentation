## Keyrings

Public keys, encrypted messages and other OpenPGP artifacts consist of packets with a specific sequence. Certificates for instance start with a Public-Key Packet, followed by a string of packets which contain the actual content of the certificate (User IDs, expiration date, subkeys, etc.), intermingled with Signature Packets which bind everything together. 

A stream of these packets can contain multiple artifacts. If such a stream consists only of certificates (with or without secret key material), such a stream is called a *keyring*.

Keyrings are a way to store a collection of certificates in a single file.

### Creating a keyring

A keyring can be created by exporting certificates from the cert store:

```shell
$ sq cert export --cert $FPR_A --cert $FPR_B --cert $FPR_C ... > $FILE
```

Likewise `sq key export ...` will also create a keyring.

### Listing the certificates in a keyring

The summary of certificates in a keyring can be listed by:

```shell
$ sq keyring list example_keyring.pgp
0. 159F7D1D498EF2BFC489D13FB5A86AAD248300C8 Bob
1. 722E3D4101B82C0AE80A3EF07A512226FF0D14E7 Alice
2. 8B19816EE960EEBBD09A1256CF755B2FB77C6171 Dave
3. CA0CB6126046DE2E91770FDBBBC18569610E3ACF Carol
```

### Extracting certificates from a keyring

Certificates can be extracted from a keyring by "filtering" it. `sq keyring filter ...` takes a keyring as input and produces a new keyring containing the certificates which match the filter.

```shell
$ sq keyring filter --experimental --userid Alice example_keyring.pgp
-----BEGIN PGP PUBLIC KEY BLOCK-----

xjMEZ7Wz3hYJKwYBBAHaRw8BAQdAuhfXxD0ewpB0BrkDpMH5YhmobPkp/eGpJRxu
[...]
=adZu
-----END PGP PUBLIC KEY BLOCK-----
```

Please note that you have to pass `--experimental` as a parameter to the command, as its API isn't stable yet and might change in the future.

Filters can be combined:

```shell
$ sq keyring filter --experimental --userid Alice --userid Bob example_keyring.pgp
```

You can filter by
- `--userid`
- `--name`
- `--email`
- `--domain` - `--domain` applies to User IDs containing an email address.
- `--cert` or `--key` - `--key` matches certificates containing secret key material.

`sq keyring filter` builds the union set of all supplied filters - the result contains all certificates which match at least one of the filters. 

The selection can be reversed by passing `--prune-certs`. The result will then contain all certificates *not* matching at least one of the filters.

```shell
$ sq keyring filter --experimental --userid Alice --prune-certs example_keyring.pgp
```
Taken the above example keyring, the result will contain the certificates of Bob, Carol and Dave, but not Alice.

If you want to build the intersection of several filters, you have to chain `sq keyring filter`:

```shell
$ sq keyring filter --experimental --domain example.com example_keyring | sq keyring filter --experimental --userid Alice
```


### Split and merge

A keyring can be split up into its certificates by:

```shell
$ sq keyring split $FILE
```

Taking the above keyring:
```shell
$ mkdir out
$ cd out
$ sq keyring split ../example_keyring.pgp 
$ ls
example_keyring.pgp-0-159F7D1D498EF2BFC489D13FB5A86AAD248300C8-bob@example.com
example_keyring.pgp-1-722E3D4101B82C0AE80A3EF07A512226FF0D14E7-alice@example.com
example_keyring.pgp-2-8B19816EE960EEBBD09A1256CF755B2FB77C6171-dave@example.com
example_keyring.pgp-3-CA0CB6126046DE2E91770FDBBBC18569610E3ACF-carol@example.com
$
```
This operation can be reversed using `sq keyring merge ...`:

```shell
$ sq keyring merge *
-----BEGIN PGP PUBLIC KEY BLOCK-----

xjMEZ7Wz3hYJKwYBBAHaRw8BAQdAuhfXxD0ewpB0BrkDpMH5YhmobPkp/eGpJRxu
[...]
=MuLv
-----END PGP PUBLIC KEY BLOCK-----
```

or any other selection of files.

```shell
$ sq keyring merge *alice* *dave* | sq keyring list
0. 722E3D4101B82C0AE80A3EF07A512226FF0D14E7 Alice
1. 8B19816EE960EEBBD09A1256CF755B2FB77C6171 Dave
```
