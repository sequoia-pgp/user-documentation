## How to publish a certificate to servers


### Keyservers 

To publish a certificate to the preconfigured list of keyservers, you can use the 'sq network keyserver publish' subcommand as follows:


```shell
$ sq network keyserver publish --cert $FINGERPRINT
```




### WKD

To generate a file and the directory structure to a local location from where it can be uploaded to a webserver like WKD, you can use the 'sq network wkd generate' subcommand. In this example /tmp/foo is the directory everything will be generated into, the information 'sequoia-pgp.org' excludes User IDs from certificates with other domain data from the key store 'certs.cert':


```
$ sq network wkd generate /tmp/foo sequoia-pgp.org certs.cert
```


### DANE

To publish a certificate by DNS / DANE, create a TXT record:

```shell
$ sq network dane generate sequoia-pgp.org certs.cert
```

      
> See chapter ['Keyservers'](./keyservers.md) for more options about this topics.
