## Generating subkeys and keys for special purposes


### Adding subkeys to a certificate

#### Generating new subkeys 

`sq` provides the possibility of generating additional subkeys for specified purposes and adding it to an existing certificate. The `sq key subkey add` command will generate and add a new subkey to the certificate both in once.

In contrast to generating a primary key, the capabilities of a subkey need to be determined explicitly. Depending on the purpose the subkey is to fulfill, it has to be equipped with the corresponding capabilities such as signing (`--can-sign`), authentication (`--can-authenticate`) and encryption type (see example below).

The default setting is to create a new subkey that is password protected, which is queried interactively and may differ from the password of the existing primary key. Nevertheless, it is also possible to specify the option `--without-password`.

The determination of the expiration date is also slightly different. While a primary key - unless otherwise specified - gets assigned a default value of 3 years, the subkey automatically inherits the expiration time of the associated primary key, unless one defines a different value using the option `--expiration`. The reference time can be changed by passing the `--time` argument.

Here is an example of the usage of the `sq key subkey add` command:

```shell
$ sq key subkey add --cert $FINGERPRINT --can-encrypt 'transport'
```

Note: Other values for `--can-encrypt` are 'storage' and 'universal', the latter being the default.

To see the result, just have a look at the certificate to which the subkey was added. It will be listed together will all other subkeys.

```shell
$ sq inspect --cert $FINGERPRINT`
```





#### Binding a subkey to another certificate

In addition to the option of generating new subkeys and attaching them to a certificate, you can bind an existing subkey (or key) to another certificate than the original one. This can be the need, for example, if you want to continue using an old subkey, but want to retire the associated primary key. Note that the subkey also remains linked with the original key as long as you don't revoke it.


Here is how it is done: 

```shell
$ sq key subkey bind --key $KEY_TO_BE_BINDED_FINGERPRINT --cert $FINGERPRINT
 
```


### Generating keys for special purposes

Per default sq generates a key that is usable for certification as well as for signing, authentication and encryption. Nevertheless, there is also the option to generate keys with limited functionalities. Depending on what purpose the key is supposed to serve, you have to either opt out the respective other features or specify the feature in more detail. There is also the possibility to generate subkeys for specified purposes and add it to the existing primary key like shown above.

The following example shows how to generate a key for certification only. Further options, also combinations of them, can be found using `sq key generate --help`. 

```shell
$ sq key generate --own-key --email=alice@example.com --cannot-sign --cannot-authenticate --cannot-encrypt
Please enter the password to protect key (press enter to not use a password): 
                                                  Please repeat the password: 
Certifying "<alice@example.com>" for 565C0F136EA97A9E19436CD67D697CFD08BBC29B.

Transferable Secret Key.

      Fingerprint: 565C0F136EA97A9E19436CD67D697CFD08BBC29B
  Public-key algo: EdDSA
  Public-key size: 256 bits
       Secret key: Unencrypted
    Creation time: 2024-12-05 10:03:50 UTC
  Expiration time: 2027-12-06 03:30:11 UTC (creation time + 2years 11months 30days 9h 16m 45s)
        Key flags: certification

           UserID: <alice@example.com>


Hint: Because you supplied the `--own-key` flag, the user IDs on this key have been marked as authenticated, and this key has been marked as a fully
      trusted introducer.  If that was a mistake, you can undo that with:

  $ sq pki link retract --cert=565C0F136EA97A9E19436CD67D697CFD08BBC29B --all

Hint: You can export your certificate as follows:

  $ sq cert export --cert=565C0F136EA97A9E19436CD67D697CFD08BBC29B

Hint: Once you are happy you can upload it to public directories using:

  $ sq network keyserver publish --cert=565C0F136EA97A9E19436CD67D697CFD08BBC29B



```
