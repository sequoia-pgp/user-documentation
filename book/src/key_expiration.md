## Key expiration
 
OpenPGP keys may include an expiration date. This date is included when a certificate of this key is generated. The expiration date within a certificate gives the software processing the cert a hint on how to handle it.
Email clients for instance might reject using an expired certificate to encrypt a message. Signature verifying software might display a warning, but continue to function.

The expiration date is optional. It's possible to create (or update) keys, so that they never expire. The expiration date does not imply that the certificate becomes "insecure" after that date. It's just a method to enforce an update.


There are different reasons for and effects of setting an expiration date for certificates. To answer the question whether it is an improval of security to set an expiration date, we think about possible 
threadmodels someone could face. Starting with a certificate expiring, you can always extend the certificate validity as long as you have access to the key. 

If you lose your key due to hardware loss or damage  without there being an attack and still have your revocation file stored at another place, you can simply revoke the key and the certificate, and don't have to wait for the expiration. If you  don't have a revocation file, you can at least let it expire by itself. 

If someone steels your key, the person would be able to compromise the key. Only a revocation file would help to avert the damage here too since the expiration date could also be manipulated by the attacker.

It can make sense to set different expiration dates for subkeys with different purposes. In the case that only a specific subkey gets lost or stolen, the primary key will remain valid. 

Another reason to set an expiration dates which is often mentioned is that it forces people to refresh their certificate stores or pubrings on a regular basis. In summary, one can say that more important than setting an expiration date is to store the revocation file separately from the key in another location. Nevertheless in some cases setting an expiration date could limit a possible damage.




