## How to decrypt a file


To decrypt a file, use `sq decrypt`, providing the name of the encrypted file and implicitly using a key in the keystore:

```shell
$ sq decrypt message.pgp
```

> See also: [Chapter 'Encrypt and decrypt files'](./encrypt_decrypt.md)



