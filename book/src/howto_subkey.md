## Additional subkeys

### How to generate an additional subkey

To generate and add an additional subkey to an existing certificate, see the following command, which exemplarily specifies the capability of signing and opts out the password protection: 


```shell
$ sq key subkey add --cert $FINGERPRINT --can-sign  --without-password  
```


###  How to bind an existing subkey to another certificate


```shell
$ sq key subkey bind --key $KEY_TO_BE_BINDED_FINGERPRINT --cert $FINGERPRINT
 
```


> To learn about the capabilities and other specifications of adding a subkey see [chapter 'Adding a subkey'](sq_subkey.md).
