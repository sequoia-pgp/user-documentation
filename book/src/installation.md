## Installing `sq`

### Linux

Install `sq` by using the package management of your linux distribution.

#### Debian

`sq` is part of the debian distribution since bookworm (Debian 12). Up to date versions are part of trixie (Debian 13) and following.

```shell
$ sudo apt install sq
```

#### Fedora

`sq` is part of Fedora 40 and following. To install `sq`, you can use `yum`:

```shell
$ sudo yum install sequoia-sq
```

### MacOSX

There is a package for `sq` from [homebrew](https://brew.sh):

```shell
$ brew install sequoia-sq
```

### Windows

Start by getting a decent OS from [https://www.debian.org](https://www.debian.org).

### Install from source

sq is written in Rust. To compile it, you will need a recent rust toolchain. Your linux distribution very likely already has the relevant packages. If you don't want to use it or can't (because it's too old or you are not using linux), there is also the option to get the toolchain from the rust project directly. Look here for more information: [https://www.rust-lang.org/tools/install](https://www.rust-lang.org/tools/install)

#### Install the dependencies (bookworm, Debian 12/ Ubuntu 24.04):

```shell
$ sudo apt install git clang nettle-dev pkg-config libssl-dev capnproto libsqlite3-dev
```

#### Install the dependencies (MacOS):

If you are using [homebrew](https://brew.sh), you can install the dependencies by

```shell
% brew install pkgconfig nettle openssl capnp
```

#### Install using cargo

After installing the dependencies, `sq` can be installed using cargo. Cargo will take care of downloading the source code and its dependencies (only the crates). 
Then, cargo will compile and install the resulting binary.

```shell
$ cargo install --locked sequoia-sq
```

If you are using this method, you get the latest release of `sq` installed. If you are interested in `HEAD` (the current state in the git repository), please continue reading.

#### Get the source

Checkout the sources from the Sequoia PGP repository at gitlab.com: 

```shell
$ git clone https://gitlab.com/sequoia-pgp/sequoia-sq.git
```

#### Build the binary

Change into the project directory and build the binary:

```shell
$ cd sequoia-sq
$ cargo build --release
```

The result can by found in the `./target/release` subfolder. Copy or soft-link it to a convenient location.

Use `cargo install` to compile and install in one step (recommended):

```shell
$ cargo install --locked --path .
```

`sq` will be installed in `$CARGO_HOME`, usually `~/.cargo/bin`. For further information and options consult `cargo help install`.

#### Check your installation

A simple "hello world"-like check is running:

```shell
$ sq version
sq 1.1.0
using sequoia-openpgp 1.22.0
with cryptographic backend Nettle 3.8 (Cv448: true, OCB: false)
```

Your output might be different. This check only shows that everything is in place and the binary can be executed.

#### Updating from source

If you installed from source to update `sq`, simply pull the latest changes from the repository and repeat the build. Change into the project directory and:

```shell
$ git pull
$ cargo install --locked --path .
```
