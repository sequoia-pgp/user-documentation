## How to rotate a key


To rotate a key by copying existing capabilities and issued certifications on a new one, you can use `sq key rotate` like this:



```shell
$ sq key rotate --cert $FINGERPRINT
```



### Replaying certifications

To only recreate the certifications made by the old key without generating a new key you can use `sq pki vouch replay` as shown here: 

```shell
$ sq pki vouch replay --source $SOURCE_FPR --target $TARGET_FPR
```


Detailed information about this topic can be found [here](./rotation.md).
