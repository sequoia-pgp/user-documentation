## How to encrypt a file

To encrypt a file with a certificate, use `sq encrypt` as it is shown here: 


```shell
$ sq encrypt --for-email alice@example.com message.txt
```



Encrypting a message and adding a signature works as follows:

```shell
sq encrypt --signer $FINGERPRINT --for-email alice@example.com
```



> See the complete instructions here: [Chapter 'Encrypt and decrypt files'](encrypt_decrypt.md).


