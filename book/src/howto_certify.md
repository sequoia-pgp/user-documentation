## How to certify a User ID for a certificate


To create an exportable certification for a binding between a User ID and a certificate, use `sq pki vouch certify`. Note that the updated certificate should then be sent to the certificate holder to be approved and published.

```shell
$ sq pki vouch  certify --certifier $FP_CERTIFIER --cert $FP_TO_BE_CERTIFIED --email=bob@example.org
```

> More information can be found here: [Chapter 'Certifications'](TODO)



