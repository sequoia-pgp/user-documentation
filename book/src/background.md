## Background

This chapter discusses some of underlying concepts of OpenPGP and public key cryptography in general. 

It is useful to read this chapter to fine tune your mental model of what is happening under the hood of OpenPGP. 
