## Search a certificate in the internet

Certificates are necessary for encryption or the verification of signatures. The users of certificates are usually not the ones who create them, so there emerges the problem of how to make certificates available to interested users. Since certificates in wire format are just plain files, they can be published on webservers or send as email attachments, but these are individual solutions which differ for each certificate.

There are solutions which are more generic - servers in the internet that can be queried for certificates.

The first generation of these servers simply accept uploaded certificates and have a standardized interface to search the inventory for User IDs. Anyone can upload any certificate, which makes the inventory a highly unreliable source.

Later solutions to the dissemination introduce hurdles to prevent the publishing of arbitrary certificates. As a side effect this increases the likelihood that certificates stored on these servers are authentic.

Sequoia PGP uses 4 different types of these servers:

- SKS servers: These are "first generation" servers with unrestricted publishing.
- Keyservers which require a confirmation: An email is send to each User ID in the certificate that resembles an email address containing a link which has to be clicked to acknowledge the upload. `keys.openpgp.org` works this way.
- Web Key Directories (WKD): The certificate is stored on a webserver and its URL is constructed from the User ID. As a side effect WKD cannot be queried for fingerprints. WKD implies the control of a webserver related to the mail domain of the User ID. 
- DNS-based Authentication of Named Entities (DANE): Although initially not meant for OpenPGP certificates, DANE can be used for their dissemination. DANE implies control over the DNS zone file of the mail domain.

When retrieving certificates from these servers, Sequoia PGP not only stores the answer, but also its origin. Based on the origin, Sequoia PGP assigns an "authenticity value" to the certificate ranging from 0/120 (unauthenticated) to 120/120 (fully authenticated). The values add up, certificates found on different servers gain authenticity. However, there is a cap of 40/120, which cannot be surpassed by this mechanism, thus preventing fully authenticated certificates generated simply by flooding the servers.
This trust signature concept comes from the RFC 9580 standard, which replaced RFC 4880, used by OpenPGP.

### Retrieving certificates

The easiest way to get a certificate is using the `sq network search $USER_ID` subcommand.

```shell
$ sq network search alice@example.com
Note: Created a local CA to record provenance information.
Note: See `sq pki link list --ca` and `sq pki link --help` for more information.

Importing 1 certificate into the certificate store:

  1. EE99C48D11A4BE940569C4B3919EA6F609043A04 Alice (partially authenticated, 2/120)

Imported 1 new certificates, updated 0 certificate, 0 certificates unchanged, 0 errors.

Hint: After checking that the certificate EE99C48D11A4BE940569C4B3919EA6F609043A04 really belongs to
      the stated owner, you can mark the certificate as authenticated.  Each stated user ID can be
      marked individually using:

  sq pki link add EE99C48D11A4BE940569C4B3919EA6F609043A04 --userid "<alice@example.com>"

  sq pki link add EE99C48D11A4BE940569C4B3919EA6F609043A04 --userid "Alice"

      Alternatively, all user IDs can be marked as authenticated using:

  sq pki link add EE99C48D11A4BE940569C4B3919EA6F609043A04 --all
```

Using `sq network search` this way will:
- query all default keyservers (keys.openpgp.org, mail-api.proton.me, keys.mailvelope.com, keyserver.ubuntu.com, sks.pod01.fleetstreetops.com)
- tries to download a certificate via WKD
- and queries the DNS for a DANE entry

In this example `sq network search` found exactly one certificate, which gets stored in the local certificate storage.

To keep track of the origin, Sequoia PGP creates intermediate certificate authorities, which certify the received certificate. This is basically the mechanism by which Sequoia PGP determines implicit authenticity by origin.

Each successful attempt adds 1/120 to the "authenticity value", in this case the same certificate is found on two different keyservers, which makes 2/120. 

The list of keyservers can be customized by passing `--server $URI`, this option can be used multiple times:

```shell
$ sq network search --server hkps://keys.openpgp.org --server hkps://my.own.keyserver.net alice@example.com
```

This customization however only affects the list of keyservers, WKD and DANE are still used.

Please note that server addresses passed via `--server` need to have their protocol specified (`hkps://`).

Using the `--output $FILE` option prevents `sq` from importing the certificate into the cert store, but outputs to the supplied `$FILE` instead. In this case, `sq` will also consult the local cert store for matching certificates.

```shell
$ sq network search --output alice.cert alice@example.com
```

`sq network search` takes a query as parameter. In the examples above this was a User ID in form of an email address, but it can also be a fingerprint.

```shell
$ sq network search EE99C48D11A4BE940569C4B3919EA6F609043A04 
```

will fetch Alice's certificate from the example above.

#### Surprising results

When querying network servers for certificates, it's possible that seemingly unrelated certificates are returned. This is especially true for WKD and DANE. `sq network search` will fetch these certificates, as they might be usable or even better fitting. 

Please keep in mind that fetched certificates are **not** authenticated automatically, so they will not undermine your security.

#### Fine tuning the list of sources for retrieval

`sq` allows you to restrict the sources it queries for answers: 

- `sq network keyserver search` 
- `sq network wkd search`
- `sq network dane search`

This will limit the search to the respective sources. Keep in mind that not all sources can deal with all type of queries: WKD for instance can only be queried for User IDs in the form of email addresses.

#### Authenticating

`sq network search` will download fitting certificates into the cert store. In this state the certificates are of limited use, as they do not (yet) contain authenticated User ID - certificate bindings. The certificate is treated as unauthenticated. To change this, use `sq pki link add` to add authentication to bindings. For details see chapter [Authenticate certificates](./auth_certs.md).

By the way:

```shell
$ sq network search alice@example.com
Note: Created a local CA to record provenance information.
Note: See `sq pki link list --ca` and `sq pki link --help` for more information.

Importing 1 certificate into the certificate store:

  1. EE99C48D11A4BE940569C4B3919EA6F609043A04 Alice (partially authenticated, 2/120)
     ----------------------------------------
     ^- this is the fingerprint
```
