## Import and export of certificates

Certificates can be disseminated in many possible ways: as download from a website, as an attachment to an email and so on. In these cases you end up with a certificate contained in a file. To use that, you have to import it into the certificate store by:

```shell
$ sq cert import $FILE
```

This will try to import the certificate inside $FILE. If $FILE is skipped, `sq cert import` expects the certificate via `STDIN`:

```shell
$ cat $FILE | sq cert import
```

Importing certificates from a file has no influence on any implied authenticity. It's included into the certificate store as such. Use `sq pki link add` to [attest authenticity](./auth_certs.md).

Certificates can be exported from the cert store via

```shell
$ sq cert export $QUERY
```

with `$QUERY` consisting of:

- `--cert $FPR` - a certificate designated by its fingerprint
- `--cert-email $EMAIL` - all certificates containing a user id matching `$EMAIL`
- `--cert-userid $USERID` - all certificates containing a user id `$USERID`
- `--cert-domain $DOMAIN` - all certificates containing a user id with an email address from domain `$DOAMIN`
- `--cert-grep $PATTERN` - all certificates containing a user id matching `$PATTERN`

These queries can be combined, `sq cert export` will then export any certificate matching any of these queries.

```shell
$ sq cert export --cert-email alice@example.com
$ sq cert export --cert EE99C48D11A4BE940569C4B3919EA6F609043A04 --cert-domain example.com
```

The result of the query is printed to `STDOUT`. 
