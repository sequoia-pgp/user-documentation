## Key import and export

### Import a key


To import a key from a file into the keystore, you just have to use the following command: 

```shell
$ sq key import $KEYFILE
```

`$KEYFILE` can contain more than one key, `sq key import` will import all keys it finds, skipping any certificates also in this file.

### Export a key

To export a key from the keystore into a file to for instance create a backup or import it into another software like Thunderbird, this is the suitable command:


```shell
$ sq key export --cert $FINGERPRINT > $KEYFILE
```

or 

```shell
$ sq key export --cert $FINGERPRINT --output $KEYFILE
```


You can also export a special subkey instead of the complete material of a key. For this you can use `sq key subkey export`:

```shell
$ sq key subkey export --cert $SUBKEY_FINGERPRINT > $KEYFILE
```

> Please note: If exporting an unprotected key (one without a passphrase), the exported key will also be unprotected.

`sq key export` will export the key as it is. If you want to set a password for the exported key, use:

```shell
$ sq key export ... | sq key password --cert-file - --output $EXPORTED_KEY_FILE
```


