## User IDs

OpenPGP knows User IDs - they are the human readable content of certificates (and keys) and are meant as representations of the keyholder, for instance in form of an email address. Technically, User IDs are just sequences of printable characters. Their content depends on the intended use case. To retrieve a certificate for a certain email address, the certificate contains a User ID having the said email address.

```
<alice@example.com>
```

User IDs are not limited to email addresses, they can consist of a name:

```
Alice Example
```

or a combination of both:

```
Alice Example <alice@example.com>
```

As email addresses have to be in angle brackets, they are identifiable. Other use cases may have different conventions.

> `sq` allows a shortcut. Instead of specifying an email User ID as `--userid "<alice@example.com>"`, you can use `--email "alice@example.com"`. `sq` takes care of the angle brackets.

In Public Key Cryptography User IDs are not needed. Encryption, decryption, signing and verification are not using the information supplied in a User ID. User IDs are meant as markers to allow for a key retrieval or identification without having to resort to fingerprints. As such they are included in keys and certificates - their untaintedness is ensured by signatures also contained in the key or certificate. There is no limit in the number of User IDs a key or certificate can contain. 

User IDs can be added to a key at any time, they don't have to be all present at the time of a key's generation.  

User IDs are claims - anyone can create a key containing `<alice@example.com>` as a User ID and publish its certificate. How to validate the authenticity - the soundness of the claim - is described in chapter [Authenticating certificates](./auth_certs.md).

Removing a User ID from a key is complex. While it's technically possible to strip a User ID from a key, once the corresponding certificate got disseminated, it's impossible to call that back. Then a User ID has to be revoked, it will stay in the key with a revocation certificate added. The revocation will also be part of certificates generated from that key, giving the software processing these certificates a hint on how to handle that User ID (such as ignoring it).
