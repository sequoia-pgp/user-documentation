When a new commit is added to the main branch, the book is built, and
uploaded to [https://sequoia-pgp.gitlab.io/user-documentation].
